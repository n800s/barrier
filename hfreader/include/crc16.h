#ifndef __CRC16_H
#define __CRC16_H

#include <stdint.h>

uint16_t bundle_crc16(uint8_t *data_p, unsigned short length);
uint16_t reader_crc16(uint8_t *data_p, unsigned short length);

#endif //__CRC16_H
