#ifndef __IFACE_H

#define __IFACE_H

#define IPREFIX "IFACE:"
#define IERROR "IERROR:"

extern bool ifacemode;

void ifprintf(const char *format, ...);
void iferrorf(const char *format, ...);

#endif //__IFACE_H
