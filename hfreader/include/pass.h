#ifndef _PASS_H

#define _PASS_H

extern const char *passlist[];
extern const int passcount;

const char *rand_pass(int old_passindex=-1);
int rand_passindex(int old_passindex=-1);

#endif //_PASS_H
