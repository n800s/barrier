#ifndef _EXC_H

#define _EXC_H

#include <exception>
#include <cstdarg>
#include "debug.h"

class hfreption: public std::exception
{
	public:

		inline hfreption(const char * format, ...) {
			va_list args;
			va_start(args, format);
			vsnprintf(error_msg, sizeof(error_msg), format, args);
			dbprintf("%s\n", error_msg);
			va_end(args);
		}

		inline virtual const char * what() const throw()
		{
			return error_msg;
		}

	private:
		char error_msg[256];

};


#endif //_EXC_H
