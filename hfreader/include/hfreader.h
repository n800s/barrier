#ifndef _HFREADER_H

#define _HFREADER_H

#include "carto.h"
#include <string>
#include <stdint.h>
#include <termios.h>

#define START_READER \
	HFReader r(serial_device, reader_address, baudrate, uart_read_delay_step); \
	r.open_device();


class HFReader {
	private:
		std::string device;
		int address;
		int fd;
		int baudrate;
		int uart_read_delay_step;
		struct termios old_options;
	protected:
		void set_options(int vmin=1);
		int send_request(uint8_t addr, uint8_t cmd, uint8_t state, bool throw_exc=true, uint8_t datalen=0, const uint8_t *data=NULL, uint8_t *replybuf=NULL, int buflen=0, int retry_count=2);
		void load_info();
		void openRF();
		void closeRF();
		void test_throw_exception(const uint8_t *reply, int replylen, int cmd);
		void open_port();
		void close_port();
	public:
		struct {
			char version[10];
			int rfu;
			int type;
			char type_str[20];
			int tr_type;
			int scan_time;
		} reader;
		HFReader(const char *device, int address=0, int baudrate=0, int uart_read_delay_step=0);
		~HFReader();
		void open_device();
		void close_device();
		bool tr_init_v1(uint64_t &uid, const char key[], CARD_TYPE ctype=CT_NONE, CARD_SUBTYPE csubtype=CST_NONE, long value=0, time_t tbegin=0, time_t tend=0);
		void tr_bundle_read(const uint64_t &uid, const char *passes[], int passcount, DataBundle &bundle);
		bool tr_select(uint64_t &uid);
		void tr_unselect();

		bool selected_erase_block(uint8_t bnum);
		bool selected_read_block(uint8_t bnum, uint8_t *block);
		void selected_read_firstblock(FirstBlock &fb);
		void selected_write_firstblock(FirstBlock &fb);
		void selected_write_carto(const char key[], DataBundle &db);
		void selected_read_carto(const char *passes[], int passcount, DataBundle &db);
		void selected_write_total(const char key[], TotalBlock &tb);
		void selected_read_total(const char key[], TotalBlock &tb);
		void reset2ready();

		void setScanTime(uint8_t ms);
		void setLED(uint8_t bitmap);
		void setAddress(uint8_t address);
		int getActiveANT();
		int getPwr();
		int calibratePwr();
};

#endif //_HFREADER_H
