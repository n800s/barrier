#ifndef __DEBUG_H
#define __DEBUG_H

#include <stdio.h>

extern bool debugmode;
extern bool quietmode;

void dbprintf(const char *format, ...);
void dumpbuf(const unsigned char *buf, int buflen);

FILE *log_open(const char *logfile);
void log_close();
void dblog_rollover(bool force=false);

const char *s_timestamp(const double *dt=NULL);

#endif //__DEBUG_H
