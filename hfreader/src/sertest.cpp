#include <stdio.h>
#include "serial.h"
#include <stdlib.h>

void test_port(int port)
{
	int speed= 19200;
	char buf[ 255];
	int i, len;


	printf("Open port%d, speed= %d\n", port+1, speed);
	SerialOpen( port);
#ifdef MOXA
	SerialSetMode( port, RS232_MODE);
#endif
	SerialSetSpeed( port, speed);
	buf[0] = 5;
	buf[1] = 0xFF;
	buf[2] = 2;
	buf[3] = 0xF0;
	buf[4] = 0xBA;
	buf[5] = 0x6F;
	len = 6;
	SerialWrite( port, buf, len);
	printf("sent: ");
	for( i= 0; i< len; i++)
		printf("[%2.2X]", buf[ i]);
	printf("\n");
	sleep(1);
	int n = SerialDataInInputQueue(port);
	if( n > 0) {
		len = SerialBlockRead( port, buf, n);
		printf("recv: ");
		for( i= 0; i< len; i++)
			printf("[%2.2X]", buf[ i]);
		printf("\n");
	} else {
		printf("No reply.\n");
	}
	SerialClose(port);
	printf("Close port%d\n", port+1);
}

int main( int argc, char* argv[])
{
	test_port(PORT1);
	test_port(PORT2);
	test_port(PORT3);
	test_port(PORT4);

	return 0;
}

