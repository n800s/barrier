#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "confreader.h"
#include "hfreader.h"
#include "iface.h"

int main(int argc, char * const *argv)
{
	init_configuration(argc, argv, "");
	START_READER
	printf("version=%s\n", r.reader.version);
	printf("rfu=%d\n", r.reader.rfu);
	printf("type=%s\n", r.reader.type_str);
	printf("tr type=0x%4.4X\n", r.reader.tr_type);
	printf("scan time=%dms\n", r.reader.scan_time);
	sleep(1);
// not supported
//	int ant = r.getActiveANT();
//	printf("active antenna: 0x%2.2X\n", ant);
//	printf("current power: %d\n", r.getPwr());
//	printf("current power: %d\n", r.calibratePwr());
	uint64_t uid = card_uid;
	bool rs = r.tr_select(uid);
	if(rs) {
		ifprintf("UID=%.16llX", uid);
		for(int i=0; i< 100; i++) {
			uint8_t block[BLOCK_SIZE];
			if(r.selected_read_block(i, block)) {
				printf("Block %2.2d: ", i);
				for(int j=0; j< BLOCK_SIZE; j++) {
					printf("%.2X ", block[j]);
				}
				printf("\n");
			} else {
				printf("Done\n");
				break;
			}
		}
	} else {
		ifprintf("NOT FOUND");
	}
	r.tr_unselect();
	r.close_device();
	return rs;
}
