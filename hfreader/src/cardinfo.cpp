#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <time.h>
#include <unistd.h>
#include "carto.h"
#include "pass.h"
#include "iface.h"
#include "hfreader.h"
#include "debug.h"
#include "confreader.h"
#include "exc.h"
#include "errcode.h"

#define MIN_DURATION 40

int main(int argc, char *const *argv)
{
	init_configuration(argc, argv);
	int rs = 0;
	try {
		START_READER
		uint64_t uid = card_uid;
		if(r.tr_select(uid)) {
			printf("UID: %.16llX\n", uid);
			ifprintf("UID=%.16llX", uid);
			DataBundle bundle;
			dbprintf("pass count = %d\n", passcount);
			r.selected_read_carto(passlist, passcount, bundle);
			if(bundle.mem.carto.d.v1.uid == uid) {
				const char *sctype;
				switch(bundle.mem.carto.d.v1.ctype) {
					case CT_ADULT:
						sctype = "adult";
						break;
					case CT_CHILD:
						sctype = "child";
						break;
					default:
						sctype = "unknown";
						break;
				}
				const char *scsubtype;
				switch(bundle.mem.carto.d.v1.csubtype) {
					case CST_PERIODONLY:
						scsubtype = "periodonly";
						break;
					default:
						scsubtype = "";
						break;
				}
				printf("Pass Index=%d\n", bundle.passindex);
				printf("Maximum Value=%d\n", bundle.mem.carto.d.v1.maxval);
				printf("Start time=%s\n", time2str(bundle.mem.carto.d.v1.tbegin));
				printf("End time=%s\n", time2str(bundle.mem.carto.d.v1.tend));
				printf("Type=%s\n", sctype);
				printf("Subtype=%s\n", scsubtype);
				TotalBlock tb;
				r.selected_read_total(passlist[bundle.passindex], tb);
				printf("Value=%d\n",tb.d.v.val);
				ifprintf("MAXVALUE=%d", bundle.mem.carto.d.v1.maxval);
				ifprintf("STARTTIME=%d", bundle.mem.carto.d.v1.tbegin);
				ifprintf("ENDTIME=%d", bundle.mem.carto.d.v1.tend);
				ifprintf("TYPE=%s", sctype);
				ifprintf("SUBTYPE=%s", scsubtype);
				ifprintf("VALUE=%d", tb.d.v.val);
			} else {
				printf("Fake card %.16llX != %.16llX\n", uid, bundle.mem.carto.d.v1.uid);
				iferrorf("FAKE\n");
				rs = ERR_FAKE_CARD;
			}
			r.tr_unselect();
		} else {
			printf("No card found.\n");
		}
	} catch(hfreption &e) {
		dbprintf("Final error: %s\n", e.what());
		rs = 1;
	}
	do_exit(rs);
}
