#include "gpio.h"
#include "debug.h"
#include "exc.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#ifdef MOXA
#warning MOXA GPIO
#include <moxadevice.h>
#elif TION_PRO28
#warning TION PRO28 GPIO
#endif

FILE *fopen_exc(const char *fname, const char *mode)
{
	FILE *f = fopen(fname, mode);
	if(!f) {
		throw hfreption("Can not open %s (mode: %s): %s\n", fname, mode, strerror(errno));
	}
	return f;
}

void set_gpio_value(int index, int value)
{
#ifdef MOXA
	int state = set_dout_state(index, value);
	switch(state) {
		case DIO_OK:
			dbprintf("Port %d set to %d\n", index, value);
			break;
		case DIO_ERROR_PORT:
			dbprintf("Port %d does not exist\n", index);
			break;
		case DIO_ERROR_CONTROL:
			dbprintf("Port %d: open or ioctl fail\n", index);
			break;
		default:
			dbprintf("Port %d: unrecognized error\n", index);
			break;
	}
#elif PI
	char pbuf[256];
	FILE *f;
	f = fopen_exc("/sys/class/gpio/export", "w");
	fprintf(f, "%d", index);
	fclose(f);
	sprintf(pbuf, "/sys/class/gpio/gpio%d/direction", index);
	usleep(200000L);
	for(int i=0; i<10; i++) {
		if(access(pbuf, W_OK) != -1) {
			break;
		}
		usleep(100000);
	}
	f = fopen_exc(pbuf, "w");
	fputs("out", f);
	fclose(f);
	sprintf(pbuf, "/sys/class/gpio/gpio%d/value", index);
	f = fopen_exc(pbuf, "w");
	fprintf(f, "%d", value);
	fclose(f);
	f = fopen_exc("/sys/class/gpio/unexport", "w");
	fprintf(f, "%d", index);
	fclose(f);
#elif TION_PRO28
/*
0 = 53 -> X22, 10
1 = 54 -> X22, 11
2 = 55 -> X22, 12
3 = 56 -> X22, 15
*/
	if(index < 53) {
		index += 53;
	}
	if(index >= 53 && index <= 56) {
		char pbuf[256];
		FILE *f;
		f = fopen_exc("/sys/class/gpio/export", "w");
		fprintf(f, "%d", index);
		fclose(f);
		sprintf(pbuf, "/sys/class/gpio/gpio%d/direction", index);
		f = fopen_exc(pbuf, "w");
		fputs("out", f);
		fclose(f);
		sprintf(pbuf, "/sys/class/gpio/gpio%d/value", index);
		f = fopen_exc(pbuf, "w");
		fprintf(f, "%d", value);
		fclose(f);
		f = fopen_exc("/sys/class/gpio/unexport", "w");
		fprintf(f, "%d", index);
		fclose(f);
	}
#endif
}

