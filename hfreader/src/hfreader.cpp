#include <errno.h>
#include <termios.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <algorithm>

#include "hfreader.h"
#include "exc.h"
#include "crc16.h"
#include "debug.h"
#include "carto.h"

#include <sys/ioctl.h>
#ifdef MOXA
#include "moxadevice.h"
#endif

// type A tag
//#define TAG_WR_STATE 1

// type B tag
#define TAG_WR_STATE 9

static struct constant {
	long speed;
	long value;
} termios_constants[] = {
	{50, B50},
	{75, B75},
	{110, B110},
	{134, B134},
	{150, B150},
	{200, B200},
	{300, B300},
	{600, B600},
	{1200, B1200},
	{1800, B1800},
	{2400, B2400},
	{4800, B4800},
	{9600, B9600},
	{19200, B19200},
	{38400, B38400},
#ifdef B57600
	{57600, B57600},
#endif
#ifdef B115200
	{115200, B115200},
#endif
#ifdef B230400
	{230400, B230400},
#endif
	{0, 0}
};

long speed_code(long baudrate)
{
	constant *p;
	int i = 0;
	long rs = 0;
	do {
		p = termios_constants + i;
		if(p->speed == baudrate) {
			rs = p->value;
			break;
		}
		i++;
	} while(p->speed != 0);
	if( rs == 0 ) {
		throw hfreption("Error. Baud rate %d is not supported\n", baudrate);
	}
	return rs;
}

void HFReader::set_options(int vmin)
{
	tcgetattr(fd, &old_options);
	struct termios options;
	memcpy(&options, &old_options, sizeof(options));

#ifdef TION_PRO2

	options.c_cflag = (speed_code(baudrate) | CS8 | CLOCAL | CREAD);
	options.c_lflag = 0;
	options.c_oflag = 0;
	options.c_iflag = 0;
	options.c_cc[VMIN] = 0;
	options.c_cc[VTIME] = 0;

#elif MOXA

	options.c_cflag = (speed_code(baudrate) | CS8 | CLOCAL | CREAD);
	options.c_lflag = 0;
	options.c_oflag = 0;
	options.c_iflag = 0;
	options.c_cc[VMIN] = 1;
	options.c_cc[VTIME] = 0;

#else

	options.c_cflag = (speed_code(baudrate) | CS8 | CLOCAL | CREAD);
	options.c_lflag = 0;
	options.c_oflag = 0;
	options.c_iflag = 0;
	options.c_cc[VMIN] = 1;
	options.c_cc[VTIME] = 0;


/*	options.c_cflag |= (speed_code(baudrate) | CS8 | CLOCAL | CREAD);
	//settings stolen from pyserial
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ISIG | IEXTEN | ECHOKE | ECHOCTL);
	options.c_oflag &= ~(OPOST);
	options.c_iflag &= ~(INLCR | IGNCR | ICRNL | IGNBRK);
	options.c_cc[VMIN] = vmin;
	options.c_cc[VTIME] = 10;
*/

#endif

	cfsetispeed( &options, speed_code(baudrate));
	cfsetospeed( &options, speed_code(baudrate));

//	options.c_cflag &= ~CSTOPB; // 1 stop bit
	
//	tcflush(fd, TCIFLUSH);
//	tcsetattr(fd, TCSAFLUSH, &options);
	tcsetattr(fd, TCSANOW, &options);
//	fsync(fd);
	tcflush(fd, TCIOFLUSH);
}

HFReader::HFReader(const char *device, int address, int baudrate, int uart_read_delay_step):
	device(device), address(address), fd(-1), baudrate(baudrate), uart_read_delay_step(uart_read_delay_step)
{
	memset(&reader, 0, sizeof(reader));
	dbprintf("Will use device %s\n", this->device.c_str());
}

HFReader::~HFReader()
{

	close_device();
}

void HFReader::open_port()
{
	if(fd>=0) {
		close_port();
	}
	fd=open(device.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
//	fd=open(device.c_str(), O_RDWR | O_NDELAY);
	if(fd<0) {
		throw hfreption("Device %s open error %s", device.c_str(), strerror(errno));
	}
#ifdef MOXA
	int mode = RS232_MODE;
	ioctl( fd, MOXA_SET_OP_MODE, &mode);
#endif
	set_options();
}

void HFReader::close_port()
{
	if(fd>=0)
	{
		tcsetattr(fd, TCSANOW, &old_options);
		if(close(fd) < 0) {
			/* Error handling. See errno. */
			fprintf(stderr, "error closing...%s\n", strerror(errno));
		}
		fd = -1;
	}
}


void HFReader::open_device()
{
	openRF();
	load_info();
}

void HFReader::close_device()
{
	try {
		closeRF();
	} catch(...) {
	}
}

// return number of bytes read
int HFReader::send_request(uint8_t addr, uint8_t cmd, uint8_t state, bool throw_exc, uint8_t datalen, const uint8_t *data, uint8_t *replybuf, int buflen, int retry_count)
{
	int n, rs = 0, attempt=0;
	uint8_t cmdbuf[100];
	uint8_t rlen;
	uint16_t crc, reply_crc;
	int tdelay;
	while(attempt < retry_count) {
		try {
			open_port();
			cmdbuf[0] = 3 + datalen + 2;
			cmdbuf[1] = addr;
			cmdbuf[2] = cmd;
			cmdbuf[3] = state;
			if(data && datalen) {
				memcpy(cmdbuf + 4, data, datalen);
			}
			crc = reader_crc16(cmdbuf, 4 + datalen);
			memcpy(cmdbuf + 4 + datalen, &crc, 2);
			dbprintf(">>> Sending command %2.2X to %2.2X, %d bytes:\n", cmd, addr, cmdbuf[0] + 1);
			dumpbuf(cmdbuf, cmdbuf[0] + 1);
			n = write(fd, cmdbuf, cmdbuf[0] + 1);
			int bytes, i;
			for(i=0; i<100; i++) {
				ioctl(fd, TIOCOUTQ, &bytes);
				if(bytes == 0) break;
				usleep(uart_read_delay_step * 1000);
			};
			tcflush(fd, TCOFLUSH);
			if(bytes > 0) {
				throw hfreption("Failed writing to the port. %d byte(s) remaining unwritten\n", bytes);
			}
//			tcdrain(fd);
//			dbprintf("Sent %d byte(s)\n", n);
			tdelay = 0;
			rlen = 0;
			for(int i=0; i<100; i++) {
				n = read(fd, &rlen, 1);
				if(n == 1) {
					break;
				}
				if(n<0 && errno != EAGAIN) {
					throw hfreption("error reading the length byte ...%d: %s", errno, strerror(errno));
				}
				usleep(uart_read_delay_step * 1000);
				tdelay += uart_read_delay_step;
			}
			if(rlen == 0) {
				throw hfreption("No reply received (total delay: %d ms)", tdelay);
			}
			uint8_t rcv[rlen + 1];
			rcv[0] = rlen;
			uint8_t n_read = 0;
			while(n_read < rlen) {
				n = read(fd, rcv+1+n_read, rlen-n_read+1);
				if(n < 0) {
					if(errno != EAGAIN) {
						throw hfreption("error reading ...%d: %s", errno, strerror(errno));
					} else {
						usleep(uart_read_delay_step * 1000);
						tdelay += uart_read_delay_step;
						if(tdelay > 100) {
							dumpbuf(rcv, n_read+1);
							crc = reader_crc16(rcv, n_read-1);
							memcpy(&reply_crc, rcv+n_read-1, sizeof(reply_crc));
							throw hfreption("Reply is too short (Total: %d bytes, %d needed, timeout: %d ms, %4.4X vs %4.4X)", n_read+1, rlen+1, tdelay, reply_crc, crc);
						}
					}
				} else {
					n_read += n;
				}
			}
			dbprintf("Reply has %d bytes (total delay %d ms):\n", rlen, tdelay);
			dumpbuf(rcv, rlen+1);
			if(rlen != rcv[0]) {
				throw hfreption("Wrong reply length: %d, expected %d", rcv[0], rlen);
			}
			rlen++;
			if( rlen < 4 ) {
				throw hfreption("Reply is too short (%d bytes)", rlen);
			} else {
				crc = reader_crc16(rcv, rlen-2);
//				does not work for moxa
//				uint16_t reply_crc = *((uint16_t *)(rcv + rlen - 2));
				memcpy(&reply_crc, rcv + rlen - 2, sizeof(reply_crc));
				if(crc != reply_crc) {
					throw hfreption("CRC error. Reply size:%d. CRC:(%4.4X) vs REPLY(%4.4X)", rlen, crc, reply_crc);
				}
				if(throw_exc) {
					test_throw_exception(rcv, rlen, cmd);
				}
				if(replybuf && buflen) {
					if( rlen > buflen ) {
						throw hfreption("Reply size (%d bytes) is bigger than buffer size (%d bytes)", rlen, buflen);
					}
					memcpy(replybuf, rcv, rlen);
				}
				rs = rlen;
			}
//			usleep(uart_read_delay_step * 100000);
			close_port();
			break;
		} catch(hfreption &e) {
			if(++attempt >= retry_count)
				throw;
			dbprintf("**** Attempt %d ****\n", attempt+1);
			sleep(1);
		}
	}
	return rs;
}

void HFReader::test_throw_exception(const uint8_t *reply, int replylen, int cmd)
{
	switch(reply[2]) {
		case 0:
			break;
		case 1:
			throw hfreption("Command %2.2X operand length error", cmd);
		case 2:
			throw hfreption("Command 0x%2.2X not supported", cmd);
		case 3:
			throw hfreption("Command %d operand out of range", cmd);
		case 5:
			throw hfreption("Command %2.2X. RF is off. Reply size:%d. status:%2.2X", cmd, replylen, reply[2]);
		case 6:
			throw hfreption("Command %2.2X. EEPROM operation error.", cmd);
		case 0x0a:
			throw hfreption("Command %2.2X. Inventory scan time overflow.", cmd);
		case 0x0b:
			throw hfreption("Command %2.2X. Inventory not finished before scan time overflow.", cmd);
		case 0x0c:
			throw hfreption("Command %2.2X. ISO error.", cmd);
		case 0x0e:
			throw hfreption("Command %2.2X. No tags.", cmd);
		case 0x0f:
			throw hfreption("Command %2.2X. Operation Error %d.", cmd, reply[3]);
		default:
			throw hfreption("Status error. Reply size:%d. status:%2.2X", replylen, reply[2]);
	}
}


static const char *readerTypeStr(uint8_t reader_type)
{
	switch(reader_type) {
		case 0x45:
			return "HF9001";
		case 0x55:
			return "HF9036";
		case 0x58:
			return "HF9038";
		case 0x5C:
			return "HF9036SC";
		case 0x46:
			return "HF9001Lite";
		case 0x47:
			return "HF9003A";
		case 0x71:
			return "HF9003AD";
		case 0x48:
			return "HF9036CF";
		case 0xA1:
			return "HF9201";
		case 0xA2:
			return "HF9203A";
		case 0xA6:
			return "HF9201Lite";
		case 0x80:
			return "HF9091D";
		case 0x5B:
			return "HF9037";
		case 0x5D:
			return "HF9037SC";
		case 0x90:
			return "HF9291";
		case 0x30:
			return "HF9091F";
		case 0x3A:
			return "HF9296";
		case 0x3B:
			return "HF9296A";
		case 0xA7:
			return "HF9201D";
		case 0x4B:
			return "HF9002AD";
		case 0xA3:
			return "HF9202AD";
		case 0x81:
			return "HF9092A";
		case 0x82:
			return "HF9093A";
		case 0x93:
			return "HF9291F";
		case 0x91:
			return "HF9292A";
		case 0x92:
			return "HF9293A";
		case 0x70:					//version 06.00
			return "HF9001D";
		case 0x8B:
			return "HF9203AGD";
		case 0x72:
			return "HF9203AD";
		case 0x97:
			return "HF9291D";
		case 0x7E:
			return "HF9001T";
		case 0x7F:
			return "HF9201T";
		case 0x8E:
			return "HF9091T";
		case 0x8F:
			return "HF9291T";
		case 0x83:
			return "HF9092T";
		case 0x84:
			return "HF9093T";
		case 0x74:
			return "HF9002T";
		case 0x73:
			return "HF9003T";
		case 0x98:
			return "HF9202T";
		case 0x99:
			return "HF9203T";
		case 0x9A:
			return "HF9292T";
		case 0x9B:
			return "HF9293T";
		case 0x8A:
			return "HF9003AGD";
		case 0x50:
			return "HF9001TSC";
		case 0x51:
			return "HF9191T";
		default:
		{
			static char rs[100];
			snprintf(rs, sizeof(rs), "UNDEFINED (%2.2X)", reader_type);
			return rs;
		}
	}
}

void HFReader::load_info()
{
	uint8_t buf[100];
	send_request(address, 0, 0xf0, true, 0, NULL, buf, sizeof(buf));
	snprintf(reader.version, sizeof(reader.version), "%2.2d.%2.2d", buf[3], buf[4]);
	reader.rfu = *(uint16_t*)(buf+5);
	reader.type = *(buf+7);
	snprintf(reader.type_str, sizeof(reader.type_str), "%s", readerTypeStr(reader.type));
	reader.tr_type = *(uint16_t*)(buf+8);
	reader.scan_time = *(buf+10);
}

void HFReader::openRF()
{
	send_request(address, 2, 0xf0);
}

void HFReader::closeRF()
{
	send_request(address, 1, 0xf0);
}

bool HFReader::tr_init_v1(uint64_t &uid, const char key[], CARD_TYPE ctype, CARD_SUBTYPE csubtype, long value, time_t tbegin, time_t tend)
{
	bool rs = tr_select(uid);
	if(rs) {
		DataBundle bundle;
		TotalBlock tb;
		bundle.mem.carto.v = 1;
		bundle.mem.carto.d.v1.uid = uid;
		bundle.mem.carto.d.v1.ctype = ctype;
		bundle.mem.carto.d.v1.csubtype = csubtype;
		bundle.mem.carto.d.v1.maxval = value;
		bundle.mem.carto.d.v1.tbegin = tbegin;
		bundle.mem.carto.d.v1.tend = tend;
		tb.d.v.val = value;
		selected_write_carto(key, bundle);
		selected_write_total(key, tb);
		TotalBlock tb1;
		selected_read_total(key, tb1);
		tr_unselect();
	}
	return rs;
}

void HFReader::tr_bundle_read(const uint64_t &uid, const char *passes[], int passcount, DataBundle &bundle)
{
	selected_read_carto(passes, passcount, bundle);
	if(bundle.mem.carto.d.v1.uid != uid) {
		throw hfreption("Fake card");
	}
}

bool HFReader::tr_select(uint64_t &uid)
{
	unsigned char buf[50];
	unsigned char mask[8];
	int rs = true;
	memset(mask, 0xff, sizeof(mask));
	if(uid) {
		dbprintf("Requested card %.16llX\n", uid);
		memcpy(buf, &uid, sizeof(uid));
	} else {
		int n = send_request(address, 1, 0, false, 0, NULL, buf, sizeof(buf));
		switch(buf[2]) {
			case 0:
				if(n == 14) {
					memcpy(&uid, buf+n-2-sizeof(uid), sizeof(uid));
					dbprintf("Inventory uid:%.16llX\n", uid);
					dbprintf("DSFID:0x%2.2X\n", buf[3]);
				} else {
					throw hfreption("Wrong reply size on inventory request (%d), expected %d", n, 14);
				}
				break;
			case 0xe:
				rs = false;
				break;
			default:
				test_throw_exception(buf, n, 1);
				rs = false;
				break;
		}
	}
	if(rs) {
		send_request(address, 0x25, 0, true, sizeof(uid), (uint8_t*)(&uid), buf, sizeof(buf));
	}
	return rs;
}

void HFReader::tr_unselect()
{
	send_request(address, 0x26, 0x01, false);
}

void HFReader::selected_read_firstblock(FirstBlock &fb)
{
	uint8_t buf[20];
	uint8_t bn = FIRST_BLOCK_OFFSET;
	dbprintf("First block offset=%d\n", FIRST_BLOCK_OFFSET);
	send_request(address, 0x20, 1, true, sizeof(bn), &bn, buf, sizeof(buf));
	fb.loc = buf[4] % 2;
	memcpy(&fb.crc, buf+5, sizeof(fb.crc));
	dbprintf("CRC read=%.4X\n", fb.crc);
}

bool HFReader::selected_erase_block(uint8_t bnum)
{
	uint8_t status[5];
	uint8_t buf[BLOCK_SIZE+1];
	buf[0] = bnum;
	memset(buf+1, 0, BLOCK_SIZE);
	send_request(address, 0x21, TAG_WR_STATE, false, sizeof(buf), buf, status, sizeof(status));
	return status[2] != 0x0e;
}

bool HFReader::selected_read_block(uint8_t bnum, uint8_t *block)
{
	uint8_t buf[20];
	uint8_t bn = bnum;
	send_request(address, 0x20, 1, false, sizeof(bn), &bn, buf, sizeof(buf));
	bool rs = buf[2] == 0;
	if(rs) {
		memcpy(block, buf+4, BLOCK_SIZE);
	}
	return rs;
}

void HFReader::selected_write_firstblock(FirstBlock &fb)
{
	uint8_t buf[BLOCK_SIZE+1];
	int msize = std::min<int>(BLOCK_SIZE, sizeof(fb));
	memcpy(buf+1, &fb, msize);
	dbprintf("CRC write=%.4X\n", fb.crc);
	srand(time(NULL));
	for(int i=msize+1; i<sizeof(buf); i++) {
		buf[i] = rand() % 256;
	}
	buf[1] = (buf[1]) ? (rand() % 256 | 1) : (rand() % 256 & 0xfe);
	buf[0] = FIRST_BLOCK_OFFSET;
	send_request(address, 0x21, TAG_WR_STATE, true, sizeof(buf), buf);
}

void HFReader::selected_write_carto(const char key[], DataBundle &db)
{
	FirstBlock fb;
	selected_read_firstblock(fb);
	fb.loc = !fb.loc;
	dbprintf("location=%ld\n", fb.loc);
	int startblock = (fb.loc) ? BUNDLE_OFFSET + MAX_BLOCK_COUNT : BUNDLE_OFFSET;
	fb.crc = db.calc_crc();
	dbprintf("Write:\n");
	dumpbuf(db.mem.buf, sizeof(db.mem.buf));
	dbprintf("write to startblock:%d\n", startblock);
	bf_encrypt(key, db.mem);
	time_t tb = time(NULL);
	uint8_t block[BLOCK_SIZE+1];
	dbprintf("Writing buf:\n");
	dumpbuf(db.mem.buf, MAX_BLOCK_COUNT * BLOCK_SIZE);
	for(int i=0; i<MAX_BLOCK_COUNT; i++) {
		block[0] = startblock + i;
		memcpy(block+1, db.mem.blocks + i, 4);
		send_request(address, 0x21, TAG_WR_STATE, true, sizeof(block), block);
		dbprintf("%d (block %d) write finished\n", i, block[0]);
	}
	selected_write_firstblock(fb);
	dbprintf("write first block finished\n");
	//write garbage to another location
	startblock = (fb.loc) ? BUNDLE_OFFSET : BUNDLE_OFFSET + MAX_BLOCK_COUNT;
	for(int i=0; i<MAX_BLOCK_COUNT; i++) {
		for(int j=1; j<=BLOCK_SIZE; j++) {
			block[j] = rand() % 256;
		}
		block[0] = startblock + i;
		send_request(address, 0x21, TAG_WR_STATE, true, sizeof(block), block);
		dbprintf("write garbage to block %d finished\n", startblock+i);
	}
	//write garbage to remaining blocks
	int ret, i = BUNDLE_OFFSET + 2 * MAX_BLOCK_COUNT;
	do {
		for(int j=1; j<=BLOCK_SIZE; j++) {
			block[j] = rand() % 256;
		}
		block[0] = i;
		send_request(address, 0x21, TAG_WR_STATE, true, sizeof(block), block);
		dbprintf("write garbage to block %d finished\n", i);
		i++;
	} while(ret == 0);
}

void HFReader::selected_read_carto(const char *passes[], int passcount, DataBundle &db)
{
	FirstBlock fb;
	selected_read_firstblock(fb);
	dbprintf("location=%ld\n", fb.loc);
	int startblock = (fb.loc) ? BUNDLE_OFFSET + MAX_BLOCK_COUNT : BUNDLE_OFFSET;
	uint8_t data[2];
	data[0] = startblock;
	data[1] = MAX_BLOCK_COUNT;
	uint8_t buf[3 + 5 * MAX_BLOCK_COUNT + 2];
	dbprintf("read %d blocks starting from %d\n", MAX_BLOCK_COUNT, startblock);
	send_request(address, 0x23, 1, true, sizeof(data), data, buf, sizeof(buf));
	for(int i=0; i<MAX_BLOCK_COUNT; i++) {
		memcpy(db.mem.buf + i * BLOCK_SIZE, buf+3+i*(BLOCK_SIZE+1)+1, BLOCK_SIZE);
	}
//	dbprintf("Read buf:\n");
//	dumpbuf(db.mem.buf, MAX_BLOCK_COUNT * BLOCK_SIZE);
	db.passindex = -1;
	for(int i=0; i<passcount; i++) {
		db.backup();
		bf_decrypt(passes[i], db.mem);
//		dbprintf("Read time %d:\n", i);
//		dumpbuf(db.mem.buf, sizeof(db.mem.buf));
		int calc_crc = db.calc_crc();
		if( calc_crc != fb.crc ) {
			db.restore();
		} else {
			db.passindex = i;
			break;
		}
	}
	if(db.passindex < 0) {
		throw hfreption("No decriptor for read data found. CRC error.");
	}
}

void HFReader::selected_write_total(const char key[], TotalBlock &tb)
{
	dbprintf("Writing value=%d\n", tb.d.v.val);
	tb.d.v.inv = -tb.d.v.val;
	bf_encrypt(key, tb);
	uint8_t buf[BLOCK_SIZE+1];
	int msize = std::min<int>(BLOCK_SIZE, sizeof(tb.d.buf));
	memcpy(buf+1, tb.d.buf, msize);
	buf[0] = TOTAL_BLOCK_OFFSET;
	send_request(address, 0x21, TAG_WR_STATE, true, sizeof(buf), buf);
}

void HFReader::selected_read_total(const char key[], TotalBlock &tb)
{
	uint8_t buf[20];
	uint8_t bn = TOTAL_BLOCK_OFFSET;
	send_request(address, 0x20, 1, true, sizeof(bn), &bn, buf, sizeof(buf));
	memcpy(tb.d.buf, buf+4, sizeof(tb.d.buf));
	bf_decrypt(key, tb);
	dbprintf("Read value=%d, crc=%d, expr=%d\n", tb.d.v.val, tb.d.v.inv, (uint16_t)(tb.d.v.val + tb.d.v.inv));
	if(((uint16_t)(tb.d.v.val + tb.d.v.inv)) != 0) {
		throw hfreption("Reading total CRC error");
	}
}

void HFReader::reset2ready()
{
	send_request(address, 0x26, 0x01, false);
}

void HFReader::setLED(uint8_t bitmap)
{
	send_request(address, 0x05, 0xf0, true, sizeof(bitmap), &bitmap);
}

void HFReader::setAddress(uint8_t address)
{
	send_request(address, 0x03, 0xf0, true, sizeof(address), &address);
}

int HFReader::getActiveANT()
{
	uint8_t rs;
	send_request(address, 0x09, 0xf0, true, 0, NULL, &rs, sizeof(rs));
	return rs;
}

int HFReader::getPwr()
{
	uint8_t buf[2];
	send_request(address, 0x22, 0xf0, true, 0, NULL, buf, sizeof(buf));
	return buf[1];
}

int HFReader::calibratePwr()
{
	uint8_t rs;
	send_request(address, 0x11, 0xf0, true, 0, NULL, &rs, sizeof(rs));
	return rs;
}

void HFReader::setScanTime(uint8_t ms)
{
	send_request(address, 0x04, 0xf0, true, sizeof(ms), &ms);
}

