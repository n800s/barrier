#define __STDC_LIMIT_MACROS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <time.h>
#include <unistd.h>
#include "carto.h"
#include "pass.h"
#include "hfreader.h"
#include "iface.h"
#include "confreader.h"
#include "errcode.h"
#include "exc.h"
#include "debug.h"

int main(int argc, char * const *argv)
{
	init_configuration(argc, argv, "v:V:");
	int rs = 0;
	if(has_value2add || has_value2set) {
		try {
			srand(time(NULL));
			START_READER
			uint64_t uid = card_uid;
			if(r.tr_select(uid)) {
				ifprintf("UID=%.16llX", uid);
				DataBundle bundle;
				int passindex;
				r.selected_read_carto(passlist, passcount, bundle);
				if(bundle.mem.carto.d.v1.uid == uid) {
					TotalBlock tb;
					r.selected_read_total(passlist[bundle.passindex], tb);
					if(has_value2set) {
						if(value2set >= INT16_MIN && value2set <= INT16_MAX) {
							tb.d.v.val = value2set;
						} else {
							iferrorf("Value (%ld) can not be bigger than %d and smaller than %d", value2set, INT16_MAX, INT16_MIN);
							rs = ERR_VAL_LIMIT;
						}
					}
					if(has_value2add) {
						long totval = tb.d.v.val + value2add;
						if(totval >= INT16_MIN && totval <= INT16_MAX) {
							tb.d.v.val = totval;
						} else {
							iferrorf("Total value (%ld) can not be bigger than %d and smaller than %d", totval, INT16_MAX, INT16_MIN);
							rs = ERR_VAL_LIMIT;
						}
					}
					if(rs == 0) {
						if(tb.d.v.val > bundle.mem.carto.d.v1.maxval) {
							//let's make maximum value == current value
							bundle.mem.carto.d.v1.maxval = tb.d.v.val;
							r.selected_write_carto(passlist[bundle.passindex], bundle);
						}
						r.selected_write_total(passlist[bundle.passindex], tb);
						TotalBlock tbr;
						r.selected_read_total(passlist[bundle.passindex], tbr);
						printf("Written value=%d\n",tbr.d.v.val);
						ifprintf("VALUE=%d",tbr.d.v.val);
					}
				} else {
					printf("Fake card\n");
					rs = ERR_FAKE_CARD;
				}
				r.tr_unselect();
			}
		} catch(hfreption &e) {
			dbprintf("%s\n", e.what());
			rs = 1;
		}
	} else {
		printf("Usage: addvalue [-v <value to add>] [-V <value to set>]\n");
		rs = ERR_USAGE;
	}
	do_exit(rs);
}
