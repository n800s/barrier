#include "crc16.h"

#define POLYNOMIAL 0x8408
#define PRESET_VALUE 0xffff

uint16_t reader_crc16(uint8_t *data_p, unsigned short length)
{
	int i,j;
	uint16_t crc=PRESET_VALUE;

	for(i=0; i<length; i++) {
		crc = crc ^ data_p[i];
		for(j=0; j<8; j++) {
			if(crc & 1) {
				crc = (crc >> 1) ^ POLYNOMIAL;
			} else {
				crc=(crc>>1);
			}
		}
	}
	return crc;
}

#define POLY 0x8408

uint16_t bundle_crc16(uint8_t *data_p, unsigned short length)
{
	unsigned int i;
	unsigned int data;
	unsigned int crc = 0xffff;

	if (length == 0)
		return (~crc);

	do {
		for (i = 0, data = (unsigned int)0xff & *data_p++; i < 8; i++, data >>= 1) {
			if ((crc & 0x0001) ^ (data & 0x0001))
				crc = (crc >> 1) ^ POLY;
			else
				crc >>= 1;
		}
	} while (--length);
 
	crc = ~crc;
 
	data = crc;
	crc = (crc << 8) | (data >> 8 & 0xFF);
 
	return (crc);
}
