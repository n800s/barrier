#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/param.h>
#include "carto.h"
#include "crc16.h"
#include "debug.h"
#include <openssl/aes.h>
#include "aescrypt.h"
#include "descrypt.h"

//check for maximum size of CARTO that can not be bigger than MAX_BLOCK_COUNT*BLOCK_SIZE
char size_test[MAX_BLOCK_COUNT*BLOCK_SIZE - sizeof(CARTO)];
//check for number of blocks divided by AES_BLOCK_SIZE without remaining
char size_test1[MAX_BLOCK_COUNT*BLOCK_SIZE / AES_BLOCK_SIZE];
//the array is not used in the code so far

uint16_t DataBundle::calc_crc()
{
//	dbprintf("calculating crc for\n");
//	dumpbuf(mem.buf, sizeof(mem.buf));
	uint16_t rs = bundle_crc16(mem.buf, sizeof(mem.buf));
	dbprintf("calculated crc=%.4X\n", rs);
	return rs;
}

#define P_FLAGS 0x12

void bf_encrypt(const char *key, CARTO_MEM &m)
{
	process(true, (const uint8_t*)key, strlen(key), m.buf, sizeof(m.buf));
}

void bf_decrypt(const char *key, CARTO_MEM &m)
{
	process(false, (const uint8_t*)key, strlen(key), m.buf, sizeof(m.buf));
}

void bf_encrypt(const char *key, TotalBlock &m)
{
	DESEncrypt((const uint8_t*)key, m.d.buf, sizeof(m.d.buf));
}

void bf_decrypt(const char *key, TotalBlock &m)
{
	DESDecrypt((const uint8_t*)key, m.d.buf, sizeof(m.d.buf));
}

const char *time2str(uint32_t timestamp) {
	time_t tt = timestamp;
	char *ds = asctime(localtime(&tt));
	ds[strlen(ds) - 1] = 0;
	return ds;
}
