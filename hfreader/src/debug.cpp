#include "debug.h"
#include "confreader.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <glob.h>
#include <sys/time.h>

bool debugmode=0;

bool quietmode = false;

FILE *flog=NULL;
char log_filename[256];

FILE *log_open(const char *logfile)
{
	if(max_log_count > 0) {
		char pattern[300];
		glob_t globbuf;
		snprintf(pattern, sizeof(pattern), "%s.*.arc", logfile);
		glob(pattern, 0, NULL, &globbuf);
		for(int i=0; i+max_log_count < globbuf.gl_pathc+1; i++) {
			remove(globbuf.gl_pathv[i]);
		}
		globfree(&globbuf);
	}
	strncpy(log_filename, logfile, sizeof(log_filename));
	flog = fopen_split(logfile, "a", max_log_size);
	return flog;
}

void log_close()
{
	if(flog) {
		fclose(flog);
	}
}

const char *s_timestamp(const double *dt)
{
	timeval tv;
	static char rs[50];
	if(dt) {
		tv.tv_sec = (time_t)*dt;
		tv.tv_usec = (suseconds_t)(long(((*dt) * 1000000)) % 1000000);
	} else {
		gettimeofday(&tv, NULL);
	}
	struct tm *st = localtime(&tv.tv_sec);
	sprintf(rs, "%.4d-%.2d-%.2d %.2d:%.2d:%.2d.%6.6ld", st->tm_year+1900, st->tm_mon+1, st->tm_mday, st->tm_hour, st->tm_min, st->tm_sec, tv.tv_usec);
	return rs;
}

void dbprintf(const char *format, ...)
{
	static bool add_date = true;
	va_list va;
	va_start(va, format);
	if(debugmode) {
		vfprintf(stderr, format, va);
	}
	if(flog) {
		if(add_date) {
			fprintf(flog, "[%s] ", s_timestamp());
			add_date = false;
		}
		vfprintf(flog, format, va);
		if(strstr(format, "\n")) {
			add_date = true;
		}
		fflush(flog);
		dblog_rollover();
	}
	va_end(va);
}

void dumpbuf(const unsigned char *buf, int buflen)
{
	for(int i=0; i< buflen; i+=16) {
		for(int j=0; j< 16; j++) {
			if(i+j >= buflen)
				break;
			dbprintf("%.2X ", buf[j+i]);
		}
		dbprintf("\n");
	}
}

void dblog_rollover(bool force)
{
	if(flog) {
		long fsize = ftell(flog);
		if((max_log_size > 0 && fsize > max_log_size) || (force && fsize > 0) ) {
			//rollover
			log_close();
			log_open(log_filename);
		}
	} else {
		log_open(log_filename);
	}
}
