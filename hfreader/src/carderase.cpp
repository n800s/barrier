#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <time.h>
#include <unistd.h>
#include "carto.h"
#include "pass.h"
#include "hfreader.h"
#include "exc.h"
#include "confreader.h"
#include "debug.h"

int main(int argc, char *argv[])
{
	init_configuration(argc, argv, "q");
	int rs = 0;
	START_READER
	try {
		uint64_t uid = card_uid;
		if(r.tr_select(uid)) {
			for(int i=0; i< 100; i++) {
				if(r.selected_erase_block(i)) {
					if(!quietmode) {
						printf("Block %d erased\n", i);
					}
				} else {
					printf("Done\n");
					break;
				}
			}
			r.tr_unselect();
		}
	} catch(hfreption &e) {
		dbprintf("Final error: %s\n", e.what());
		rs = 1;
	}
	do_exit(rs);
}
