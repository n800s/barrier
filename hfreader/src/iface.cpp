#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "iface.h"

bool ifacemode = false;


void iferrorf(const char *format, ...)
{
	va_list va;
	va_start(va, format);
	if(ifacemode) {
		fprintf(stdout, IERROR);
		vfprintf(stdout, format, va);
		fprintf(stdout, "\n");
	} else {
		vfprintf(stderr, format, va);
		fprintf(stderr, "\n");
	}
	va_end(va);
}


void ifprintf(const char *format, ...)
{
	va_list va;
	va_start(va, format);
	if(ifacemode) {
		fprintf(stdout, IPREFIX);
		vfprintf(stdout, format, va);
		fprintf(stdout, "\n");
	}
	va_end(va);
}

