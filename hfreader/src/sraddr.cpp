#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "confreader.h"
#include "hfreader.h"
#include "debug.h"
#include "errcode.h"

int main(int argc, char * const *argv)
{
	ctype_str[0] = 0;
	init_configuration(argc, argv, "a:t:");
	int rs = ERR_USAGE;
	int scantime = atoi(ctype_str);
	START_READER
	if(new_reader_address >= 0) {
		r.setAddress(new_reader_address);
		printf("Address = %d\n", new_reader_address);
		rs = EXIT_SUCCESS;
	}
	if(scantime > 0) {
		r.setScanTime(scantime);
		printf("Scan time = %d\n", scantime);
		rs = EXIT_SUCCESS;
	}
	if(rs == ERR_USAGE) {
		printf("Usage: sraddr -a <new reader address> -t <scantime (>=30)>\n");
	}
	do_exit(rs);
	dbprintf("exiting\n");
}
