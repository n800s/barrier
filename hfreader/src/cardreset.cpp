#define __STDC_LIMIT_MACROS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <time.h>
#include <unistd.h>
#include "carto.h"
#include "pass.h"
#include "hfreader.h"
#include "confreader.h"
#include "debug.h"
#include "iface.h"
#include "errcode.h"
#include "exc.h"

int main(int argc, char * const *argv)
{
	int rs = 0;
	init_configuration(argc, argv, "t:p:V:b:e:");
	try {
		if(value2set < INT16_MIN || value2set > INT16_MAX) {
			iferrorf("Value (%ld) can not be bigger than %d and smaller than %d", value2set, INT16_MAX, INT16_MIN);
			rs = ERR_VAL_LIMIT;
		} else {
			long val;
			CARD_TYPE ct;
			if(strcmp(ctype_str, "child") == 0) {
				ct = CT_CHILD;
			} else if(strcmp(ctype_str, "adult") == 0) {
				ct = CT_ADULT;
			} else {
				iferrorf("Type '%s' is wrong (adult or child)\n", ctype_str);
				ctype_str[0] = 0;
				rs = ERR_CTYPE;
			}
			CARD_SUBTYPE cst;
			if(strcmp(csubtype_str, "periodonly") == 0) {
				cst = CST_PERIODONLY;
			} else if (csubtype_str[0]) {
				cst = CST_NONE;
				iferrorf("Subtype '%s' is wrong (periodonly)\n", csubtype_str);
				csubtype_str[0] = 0;
				rs = ERR_CSUBTYPE;
			}
			if(rs == 0) {
				START_READER
				uint64_t uid = card_uid;
				if(r.tr_init_v1(uid, rand_pass(), ct, cst, val=value2set, tbegin=tbegin, tend=tend)) {
					if(r.tr_select(uid)) {
						DataBundle bundle;
						TotalBlock tb;
						r.selected_read_carto(passlist, passcount, bundle);
						ifprintf("UID=%.16llX", bundle.mem.carto.d.v1.uid);
						ifprintf("MAXVALUE=%d", bundle.mem.carto.d.v1.maxval);
						ifprintf("STARTTIME=%s", time2str(bundle.mem.carto.d.v1.tbegin));
						ifprintf("ENDTIME=%s", time2str(bundle.mem.carto.d.v1.tend));
						ifprintf("TYPE=%s", ctype_str);
						ifprintf("SUBTYPE=%s", csubtype_str);
						r.selected_read_total(passlist[bundle.passindex], tb);
						ifprintf("VALUE=%d", tb.d.v.val);
					}
					r.tr_unselect();
				}
			}
		}
	} catch(hfreption &e) {
		dbprintf("Final error: %s\n", e.what());
		rs = 1;
	}
	do_exit(rs);
}
