#include "hfreader.h"
#include "debug.h"
#include "pass.h"
#include "confreader.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <pwd.h>
#include <signal.h>
#include "gpio.h"
#include "errcode.h"
#include "exc.h"

#define ACT_MAX_N 50
#define NUMS_MAX_N 10
#define PATTERN_SIZE 5

volatile sig_atomic_t got_usr1;

void sigusr1_handler(int sig)
{
	got_usr1 = 1;
}

void execute_event(HFReader &r, const char event[])
{
	int n_act = 0;
	char *eventbuf = strdup(event);
	char *pact[ACT_MAX_N];
	dbprintf("Starting event %s\n", event);
	pact[n_act] = strtok(eventbuf, ":");
	while (pact[n_act] != NULL && n_act < ACT_MAX_N)
	{
		pact[++n_act] = strtok(NULL, ":");
	}
	for(int i=0; i<n_act; i++) {
		char act = pact[i][0];
		char *actbuf = strdup(pact[i] + 1);
		int n_nums = 0;
		char *pnums[NUMS_MAX_N];
		pnums[n_nums] = strtok(actbuf, ",");
		while (pnums[n_nums] != NULL && n_nums < NUMS_MAX_N)
		{
			pnums[++n_nums] = strtok(NULL, ",");
		}
		long nums[NUMS_MAX_N];
		dbprintf("act=%c", act);
		for(int j=0; j<n_nums; j++) {
			nums[j] = atol(pnums[j]);
			dbprintf(" %d ",nums[j]);
		}
		dbprintf("\n");
		unsigned char pattern[PATTERN_SIZE] = {1, 1, 1, 1, 1}, patternbuf[PATTERN_SIZE];
		switch(act) {
			case 'P':
				if(n_nums > 0) {
					usleep(nums[0]);
				} else {
					usleep(1000000);
				}
				break;
			case 'H':
				if(n_nums > 0) {
					dbprintf("Set GPIO%d to HIGH\n", nums[0]);
					try {
						set_gpio_value(nums[0], 1);
					} catch(hfreption e) {
						dbprintf("Error: %s\n", e.what());
					}
				}
				break;
			case 'L':
				if(n_nums > 0) {
					dbprintf("Set GPIO%d to LOW\n", nums[0]);
					try {
						set_gpio_value(nums[0], 0);
					} catch(hfreption e) {
						dbprintf("Error: %s\n", e.what());
					}
				}
				break;
		}
		free(actbuf);
	}
	dbprintf("End of event\n");
	free(eventbuf);
	return;
}

bool do_rollover()
{
	bool rs = false;
	static int last_hour = -1;
	time_t curtime = time(NULL);
	struct tm *tm_struct = localtime(&curtime);
	if(last_hour < 0) {
		last_hour = tm_struct->tm_hour;
	} else if(everyhour_rollover_mode) {
		if(last_hour != tm_struct->tm_hour) {
			rs = true;
			last_hour = tm_struct->tm_hour;
		}
	}
	return rs;
}

#define CLSIZE 5

typedef struct __CT__ {
	uint64_t uid;
	int last_time;
} CARDTIME;

int barrier_loop(HFReader &r)
{
	int rs = EXIT_SUCCESS, passi;
	static CARDTIME cardlist[CLSIZE];
	static int clsize = 0;
	bool pass;
	bool conf_error = update_config(configfile);
	uint64_t uid;
	if(!conf_error) {
		if( adult_price == 0 ) {
			dbprintf("Adult price is zero\n");
			conf_error = true;
		}
		if( child_price == 0 ) {
			dbprintf("Child price is zero\n");
			conf_error = true;
		}
	}
	if(conf_error) {
		printf("Error reading config file\n");
		rs = ERR_USAGE;
	} else while(true) {
		try {
			r.reset2ready();
			if(got_usr1 || do_rollover()) {
				dbprintf("Starting rollover...\n");
				FILE *f;
				got_usr1 = 0;
				f = fopen_split(errfile, "a", max_error_size, NULL, true);
				if(!f) {
					fclose(f);
				}
				f = fopen_split(dbfile, "a", max_transact_size, NULL, true);
				if(!f) {
					fclose(f);
				}
				dblog_rollover(true);
				dbprintf("Rollover is over\n");
			}
			uid = 0;
			r.setLED(1);
			if(r.tr_select(uid)) {
				try {
					r.setLED(0);
					int curtime = time(NULL);
					pass = true;
					for(passi=0; passi < clsize; passi++) {
						if( (curtime - cardlist[passi].last_time) > d_timeout ) {
							cardlist[passi].uid = 0;
						}
						if(cardlist[passi].uid == uid) {
							pass = false;
							break;
						}
					}
					if(pass) {
						DataBundle bundle;
						r.tr_bundle_read(uid, passlist, passcount, bundle);
						CARD_TYPE ctype = (CARD_TYPE)bundle.mem.carto.d.v1.ctype;
						CARD_SUBTYPE csubtype = (CARD_SUBTYPE)bundle.mem.carto.d.v1.csubtype;
						int cost = 0;
						bool in_period = (bundle.mem.carto.d.v1.tbegin == 0 && bundle.mem.carto.d.v1.tend == 0 && csubtype != CST_PERIODONLY
								) || (bundle.mem.carto.d.v1.tbegin <= curtime && bundle.mem.carto.d.v1.tend >= curtime);
						time_t curtime = time(NULL);
						switch(ctype) {
							case CT_CHILD:
								dbprintf("Child card type\n");
								cost = child_price;
								break;
							case CT_ADULT:
								dbprintf("Adult card type\n");
								cost = adult_price;
								break;
							default:
								dbprintf("Unknown card type %d\n", ctype);
								break;
						}
						switch(csubtype) {
							case CST_PERIODONLY:
								dbprintf("Period only card subtype\n");
								break;
						}
						FILE *f = fopen_split(dbfile, "a", max_transact_size, "CURTIME,UID,TYPE,SUBTYPE,MAXVAL,COST,BOND,PBEGIN,PEND,INPERIOD,REMAINS,\n");
						if(!f) {
							dbprintf("Failing opening file %s\n", dbfile);
						}
						fprintf(f, "%d,\"%.16llX\",%d,%d,%d,%d,%d,%d,%d,%d,", (int)curtime, bundle.mem.carto.d.v1.uid,
							bundle.mem.carto.d.v1.ctype, bundle.mem.carto.d.v1.csubtype, bundle.mem.carto.d.v1.maxval,
							-cost, bond_value, bundle.mem.carto.d.v1.tbegin, bundle.mem.carto.d.v1.tend, in_period);
						fflush(f);
						int passcode = 0;
						if(csubtype == CST_PERIODONLY) {
							if(in_period) {
								passcode = 1;
								dbprintf("In period\n");
								execute_event(r, events[(ctype==CT_ADULT) ? ADULT_TIME_CHECK : CHILD_TIME_CHECK]);
							} else {
								dbprintf("Not in period\n");
								execute_event(r, events[ERROR_EVENT]);
							}
							fprintf(f, ",");
						} else {
							if( cost > 0 ) {
								TotalBlock tb;
								r.selected_read_total(passlist[bundle.passindex], tb);
								int val = tb.d.v.val;
								if(!in_period) {
									dbprintf("Not in period\n");
									execute_event(r, events[ERROR_EVENT]);
								} else if(tb.d.v.val < (cost + bond_value)) {
									dbprintf("Found %d NOT ENOUGH\n", tb.d.v.val);
									execute_event(r, events[ERROR_EVENT]);
								} else {
									passcode = 1;
									dbprintf("Found %d\n", tb.d.v.val);
									val = tb.d.v.val -= cost;
									dbprintf("writing %d\n", tb.d.v.val);
									r.selected_write_total(passlist[bundle.passindex], tb);
									dbprintf("written\n");
									switch(ctype) {
										case CT_CHILD:
											execute_event(r, events[CHILD_PAYMENT]);
											break;
										default:
											execute_event(r, events[ADULT_PAYMENT]);
											break;
									}
								}
								fprintf(f, "%d,", val);
							} else {
								fprintf(f, ",");
							}
						}
						fprintf(f, "%d\n", passcode);
						fclose(f);
						if(clsize < CLSIZE) {
							// add uid
							cardlist[clsize].uid = uid;
							cardlist[clsize].last_time = curtime;
							clsize++;
						} else {
							// find the oldest and replace with current
							int oldest_time = curtime;
							int oldest_pos = -1;
							for(passi=0; passi < CLSIZE; passi++) {
								if( cardlist[passi].last_time < oldest_time ) {
									oldest_time = cardlist[passi].last_time;
									oldest_pos = passi;
								}
							}
							if(oldest_pos >= 0) {
								cardlist[oldest_pos].uid = uid;
								cardlist[oldest_pos].last_time = curtime;
							}
						}
					} else {
						dbprintf("Doubling (%d seconds remaining)\n", d_timeout - (curtime - cardlist[passi].last_time));
						sleep(1);
					}
				} catch(hfreption &e) {
					dbprintf("%s\n", e.what());
					execute_event(r, events[ERROR_EVENT]);
				}
				r.tr_unselect();
			} else {
				//dbprintf("Not found\n");
			}
		} catch(hfreption &e) {
			dbprintf("Error in loop: %s\n", e.what());
			rs = 1;
			sleep(5);
		} catch(...) {
			dbprintf("Unrecognized error.\n");
			rs = 1;
			sleep(5);
		}
	}
	return rs;
}

void check_add_nl(const char *fname)
{
	//check for the last symbol in a file and if it is not \n then write \n
	FILE *f = fopen(fname, "r+");
	if(f) {
		fseek(f, -1, SEEK_END);
		int lastc = fgetc(f);
		if ( lastc != 0xA ) {
			fseek(f, 0, SEEK_END);
			const char *nl = "\n";
			int n = fwrite(nl, 1, strlen(nl), f);
			dbprintf("An end of line added to %s file\n", fname);
		}
		fclose(f);
	}
}

int main(int argc, char *const *argv)
{
	int rs = 1;
	init_configuration(argc, argv, "l:");
	struct passwd *pw = getpwnam(daemon_user);
	if(!pw) {
		printf("User %s not found\n", daemon_user);
		do_exit(EXIT_FAILURE);
	} else {
		setuid(pw->pw_uid);

		struct sigaction sa;

		got_usr1 = 0;

		sa.sa_handler = sigusr1_handler;
		sa.sa_flags = 0;
		sigemptyset(&sa.sa_mask);

		if (sigaction(SIGUSR1, &sa, NULL) == -1) {
			perror("sigaction");
			exit(1);
		}


		dbprintf("Starting...\n");
		while(true) {
			try {
				START_READER
				check_add_nl(dbfile);
				rs = barrier_loop(r);
				break;
			} catch(...) {
				dbprintf("Uncaught error!!!\n");
				sleep(2);
			}
		}
		do_exit(rs);
	}
	return rs;
}

