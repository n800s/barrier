#include "hfreader.h"
#include "debug.h"
#include "iface.h"
#include "pass.h"
#include "confreader.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pwd.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include "errcode.h"
#include "exc.h"


int informer_loop(HFReader &r)
{
	int rs = EXIT_SUCCESS;
	static uint64_t last_uid=0;
	static int last_time=0;
	printf("Hello from informer\n");
	bool conf_error = update_config(configfile);
	if(conf_error) {
		fprintf(stderr, "Error reading config file\n");
		rs = ERR_USAGE;
	} else while(true) {
		uint64_t uid = 0;
		try {
			r.setLED(0x2);
			if(r.tr_select(uid)) {
				r.setLED(0);
				int curtime = time(NULL);
				if(uid != last_uid || (curtime - last_time) > d_timeout) {
					DataBundle bundle;
					r.tr_bundle_read(uid, passlist, passcount, bundle);
					dbprintf("Found UID=%.16llX\n", bundle.mem.carto.d.v1.uid);
					CARD_TYPE ctype = (CARD_TYPE)bundle.mem.carto.d.v1.ctype;
					CARD_SUBTYPE csubtype = (CARD_SUBTYPE)bundle.mem.carto.d.v1.csubtype;
					const char *sctype;
					switch(bundle.mem.carto.d.v1.ctype) {
						case CT_ADULT:
							sctype = "adult";
							break;
						case CT_CHILD:
							sctype = "child";
							break;
						default:
							sctype = "unknown";
							break;
					}
					const char *scsubtype;
					switch(bundle.mem.carto.d.v1.csubtype) {
						case CST_PERIODONLY:
							scsubtype = "periodonly";
							break;
						default:
							scsubtype = "";
							break;
					}
					int passcode = 0;
					TotalBlock tb;
					r.selected_read_total(passlist[bundle.passindex], tb);
					ifprintf("BLOCK=BEGIN");
					ifprintf("UID=%.16llX", bundle.mem.carto.d.v1.uid);
					ifprintf("STARTTIME=%d", bundle.mem.carto.d.v1.tbegin);
					ifprintf("ENDTIME=%d", bundle.mem.carto.d.v1.tend);
					ifprintf("TYPE=%s", sctype);
					ifprintf("SUBTYPE=%s", scsubtype);
					ifprintf("VALUE=%d", tb.d.v.val);
					ifprintf("BLOCK=END");
					last_uid = uid;
					last_time = curtime;
				} else {
					dbprintf("Doubling (%d seconds remaining)\n", d_timeout - (curtime - last_time));
				}
				r.tr_unselect();
			} else {
				//dbprintf("Not found\n");
			}
		} catch(hfreption &e) {
			dbprintf("%s\n", e.what());
			sleep(5);
		}
	}
	return rs;
}

int main(int argc, char *const *argv)
{
	init_configuration(argc, argv, "l:");
	struct passwd *pw = getpwnam(daemon_user);
	if(!pw) {
		printf("User %s not found\n", daemon_user);
		do_exit(EXIT_FAILURE);
	} else {
		setuid(pw->pw_uid);

		dbprintf("Starting...\n");

		START_READER
		do_exit(informer_loop(r));
	}
}

