#include "confreader.h"
#include "debug.h"
#include "iface.h"
#include "ini.h"
#include "errcode.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>

char configfile[256] = "hfreader.conf";
int adult_price = 0;
int child_price = 0;
int bond_value = 0;
int d_timeout = 20;

long max_log_size = 0;
int max_log_count = 0;
long max_transact_size = 0;
long max_error_size = 0;
bool everyhour_rollover_mode = false;

char errfile[256] = "error.log";
char dbfile[256] = "transaction.log";
char logfile[256] = "";
bool logfile_changed = false;

char events[][100]={
	"G1,2::Z,1,2,1,2,1:H1:H2:P1000000:L1:L2", //ADULT
	"G1,2:Z,5,3,2,4,1:H1:H2:P2000000:L1:L2", //CHILD
	"G1,2:Z,2,3,2,4,1:H1:H2:P2000000:L1:L2", //ADULT_TIME_CHECK
	"G1,2:Z,2,3,2,4,1:H1:H2:P2000000:L1:L2", //CHILD_TIME_CHECK
	"R5,2:Z,5,3,2,4,1:H1:P1000000:L1"
};

// serial device with reader
char serial_device[256] = DEFAULT_DEVICE;
int baudrate = DEFAULT_BAUDRATE;
//global reader address
int reader_address = BROADCAST_ADDRESS;

char daemon_user[64] = DAEMON_USER;

int uart_read_delay_step = 10;

//card uid
//if 0 then any card
uint64_t card_uid;

//new reader address to set
int new_reader_address = -1;

//card type (adult, child)
char ctype_str[20] = "adult";

//card subtype (periodonly)
char csubtype_str[20] = "";

//value to add
long value2add = 0;
bool has_value2add = false;

//value to set
long value2set = 0;
bool has_value2set = false;

//time begin format YYYYMMDDHHMI
time_t tbegin = 0;
time_t tend = 0;

time_t timestr2time(const char *timestr)
{
	int year, month ,day, hour, min;
	time_t rawtime;
	struct tm *ti;
	char tsbuf[20];
	snprintf(tsbuf, sizeof(tsbuf),"%llu", strtoll(timestr, NULL, 10));
	sscanf(timestr, "%4d%2d%2d%2d%2d", &year, &month, &day, &hour, &min);
	if(strlen(tsbuf)!=12) {
		fprintf(stderr, "Timestamp fiormat is YYYYmmddHHMM\n");
		fprintf(stderr, "Parsed to %.4d%.2d%.2d%.2d%.2d\n", year, month, day, hour, min);
	}
	time(&rawtime);
	ti = localtime(&rawtime);
	ti->tm_year = year - 1900;
	ti->tm_mon = month - 1;
	ti->tm_mday = day;
	ti->tm_hour = hour;
	ti->tm_min = min;
	ti->tm_sec = 0;
//	printf("TIMESTAMP %s\n", asctime(ti));
	return mktime(ti);
}


int process_common_options(int argc, char *const *argv, const char *opts)
{
	int c, rs=0;
	char optbuf[100] = "?hds:il:n:";
	if(opts) {
		strncpy(optbuf + strlen(optbuf), opts, sizeof(optbuf) - strlen(optbuf) - 1);
	}
	while ( (c = getopt(argc, argv, optbuf)) != -1) {
		switch (c) {
			case 'l':
				/* Open any logs here */
				if(optarg) {
					strncpy(logfile, optarg, sizeof(logfile));
					logfile_changed = true;
				}
				break;
			case 'd':
				debugmode = true;
				break;
			case 'i':
				ifacemode = true;
				break;
			case 'q':
				quietmode = true;
				break;
			case 's':
				if(optarg) {
					strncpy(configfile, optarg, sizeof(configfile));
				}
				break;
			case 't':
				if(optarg) {
					strncpy(ctype_str, optarg, sizeof(ctype_str));
				}
				break;
			case 'p':
				if(optarg) {
					strncpy(csubtype_str, optarg, sizeof(csubtype_str));
				}
				break;
			case 'n':
				if(optarg) {
					sscanf(optarg, "%llX", &card_uid);
				}
				break;
			case 'b':
				if(optarg) {
					tbegin = timestr2time(optarg);
				}
				break;
			case 'e':
				if(optarg) {
					tend = timestr2time(optarg);
				}
				break;
			case 'v':
				if(optarg) {
					value2add = atol(optarg);
					has_value2add = true;
				}
				break;
			case 'V':
				if(optarg) {
					value2set = atol(optarg);
					has_value2set = true;
				}
				break;
			case 'a':
				if(optarg) {
					new_reader_address = atoi(optarg);
				}
				break;
			case '?':
			case 'h':
				rs = ERR_USAGE;
				break;
			default:
				printf("?? getopt returned character code 0%o ??\n", c);
				break;
		}
	}
	if (optind < argc) {
		printf("non-option ARGV-elements: ");
		while (optind < argc)
			printf ("%s ", argv[optind++]);
		printf ("\n");
		rs = ERR_USAGE;
	}
	if(rs) {
		printf("Options available:\n");
		int i = 0, olen=strlen(optbuf);
		while(i < olen) {
			char n = optbuf[i];
			char a = (i+1 < olen) ? optbuf[i+1] : 0;
			if(a == ':') {
				printf(" -%c <arg>", n);
				i++;
			} else {
				printf(" -%c", n);
			}
			i++;
		}
		printf("\n");
	}
	return rs;
}

int config(void* user, const char* section, const char* name, const char* value)
{
	if( strcmp(section, "price") == 0 ) {
		if( strcmp(name, "adult") == 0 ) {
			adult_price = atoi(value);
		} else if( strcmp(name, "child") == 0 ) {
			child_price = atoi(value);
		} else if( strcmp(name, "bond") == 0 ) {
			bond_value = atoi(value);
		}
	} else if( strcmp(section, "process") == 0 ) {
		if( strcmp(name, "same_cart_timeout") == 0 ) {
			d_timeout = atoi(value);
		} else if( strcmp(name, "max_log_size") == 0 ) {
			max_log_size = atol(value);
		} else if( strcmp(name, "max_log_count") == 0 ) {
			max_log_count = atoi(value);
		} else if( strcmp(name, "max_transact_size") == 0 ) {
			max_transact_size = atol(value);
		} else if( strcmp(name, "max_error_size") == 0 ) {
			max_error_size = atol(value);
		} else if( strcmp(name, "everyhour_rollover_mode") == 0 ) {
			everyhour_rollover_mode = atoi(value);
		}
	} else if( strcmp(section, "files") == 0 ) {
		if( strcmp(name, "dbfile") == 0 ) {
			strncpy(dbfile, value, sizeof(dbfile)-1);
			dbfile[sizeof(dbfile)-1] = 0;
		} else if( strcmp(name, "errfile") == 0 ) {
			strncpy(errfile, value, sizeof(errfile)-1);
			errfile[sizeof(errfile)-1] = 0;
		} else if( strcmp(name, "logfile") == 0 && !logfile_changed) {
			strncpy(logfile, value, sizeof(logfile)-1);
			logfile[sizeof(logfile)-1] = 0;
		}
	} else if( strcmp(section, "events") == 0 ) {
		if( strcmp(name, "child_payment") == 0 ) {
			strncpy(events[CHILD_PAYMENT], value, sizeof(events[CHILD_PAYMENT])-1);
			events[CHILD_PAYMENT][sizeof(events[CHILD_PAYMENT])-1] = 0;
		} else if( strcmp(name, "adult_payment") == 0 ) {
			strncpy(events[ADULT_PAYMENT], value, sizeof(events[ADULT_PAYMENT])-1);
			events[ADULT_PAYMENT][sizeof(events[ADULT_PAYMENT])-1] = 0;
		} else if( strcmp(name, "adult_time_check") == 0 ) {
			strncpy(events[ADULT_TIME_CHECK], value, sizeof(events[ADULT_TIME_CHECK])-1);
			events[ADULT_TIME_CHECK][sizeof(events[ADULT_TIME_CHECK])-1] = 0;
		} else if( strcmp(name, "time_check") == 0 ) {
			strncpy(events[CHILD_TIME_CHECK], value, sizeof(events[CHILD_TIME_CHECK])-1);
			events[CHILD_TIME_CHECK][sizeof(events[CHILD_TIME_CHECK])-1] = 0;
		} else if( strcmp(name, "error") == 0 ) {
			strncpy(events[ERROR_EVENT], value, sizeof(events[ERROR_EVENT])-1);
			events[ERROR_EVENT][sizeof(events[ERROR_EVENT])-1] = 0;
		}
	} else if( strcmp(section, "reader") == 0 ) {
		if( strcmp(name, "serial_device") == 0 ) {
			strncpy(serial_device, value, sizeof(serial_device));
		} else if( strcmp(name, "baudrate") == 0 ) {
			baudrate =  atoi(value);
		} else if( strcmp(name, "reader_addr") == 0 ) {
			reader_address = atoi(value);
		} else if( strcmp(name, "daemon_user") == 0 ) {
			strncpy(daemon_user, value, sizeof(daemon_user));
		} else if( strcmp(name, "uart_read_delay_step") == 0 ) {
			uart_read_delay_step = atoi(value);
		}
	}
	dbprintf("[%s]%s=%s\n", section, name, value);
	return true;
}

int update_config(const char *configfile)
{
	return ini_parse(configfile, config ,NULL);
}

int do_exit(int exitcode)
{
	dbprintf("finished: with exit code=%d\n", exitcode);
	log_close();
	if(exitcode) {
		ifprintf("EXITCODE=%d", exitcode);
		if(!debugmode) {
			fprintf(stderr, "ERROR. Run with -d to see details\n");
		}
	}
	exit(exitcode);
}

void init_configuration(int argc, char *const *argv, const char *opts)
{
	if(process_common_options(argc, argv, opts) != 0) {
		do_exit(EXIT_FAILURE);
	}
	if(update_config(configfile)) {
		do_exit(EXIT_FAILURE);
	}
	if(strlen(logfile) != 0 && log_open(logfile) == NULL) {
		iferrorf("Can not open log file %s\n", logfile);
		do_exit(EXIT_FAILURE);
	}
}

FILE *fopen_split(const char *filename, const char *mode, long max_size, const char *titles, bool force)
{
	bool new_started = false;
	struct stat filestat;
	if ( stat(filename, &filestat) >= 0 ) {
		if( (max_size > 0 && filestat.st_size > max_size) || force ) {
			//make rollover
			char rfilename[256];
			time_t rawtime = time(NULL);
			struct tm *ti = localtime(&rawtime);
			snprintf(rfilename, sizeof(rfilename), "%s.%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d.arc", filename, ti->tm_year+1900, ti->tm_mon+1, ti->tm_mday, ti->tm_hour, ti->tm_min, ti->tm_sec);
			rename(filename, rfilename);
			new_started = true;
		}
	}
	FILE *rs = fopen(filename, mode);
	if(new_started && titles) {
		fputs(titles, rs);
	}
	return rs;
}

