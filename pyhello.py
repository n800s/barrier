#!/usr/bin/env python

from serial import Serial
import array

print "hello"
ser = Serial()
ser.port     = "/dev/ttyUSB0"
ser.baudrate = 115200
ser.timeout  = 1

ser.open()

requestlist = [
  [0x02, 0x80 , 0x00 , 0x7B , 0x02 , 0x00 , 0x00 , 0xF9, 0x03],
  [0x02, 0x80 , 0x00 , 0x7C , 0x02 , 0x00 , 0x00 , 0xF9, 0x03]
]

for request in requestlist:

  wdata=array.array('B', request)
  rdata = array.array('B')


  print 'write', wdata

  ser.write(wdata.tostring())

  rdata.fromstring(ser.read(30))
  print 'read', ' '.join(['0x%2.2x' % d for d in rdata.tolist()])

ser.close()

