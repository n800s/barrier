#!/bin/bash

cd barrier

make -f Makefile.moxa

DESTDIR=/home/n800s/sshfs/moxa/root/home/reader

cd -
cd barrier/build/arm

echo 'Copying ...'
cp addvalue cardinfo dump cardreset sraddr barrier $DESTDIR
cd -
echo 'Copied'

cd barrier

echo 'Copying again ...'
cp barrierd barrier.sh barrier.conf $DESTDIR
echo 'Copied again'

cd -
echo 'Chmoding ...'
chmod +x $DESTDIR/addvalue $DESTDIR/cardinfo $DESTDIR/dump $DESTDIR/sraddr $DESTDIR/cardreset $DESTDIR/barrier $DESTDIR/barrier.sh $DESTDIR/barrierd
echo 'Chmoded'
