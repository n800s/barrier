#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include "breader.h"

int main(int argc, const char **argv)
{
	int rs = 0;
	srand(time(NULL));
	FILE *f = fopen("pass.dat", "ab");
	if(f) {
		for(int i=0; i< 48; i++) {
			fputc(rand() % 256, f);
		}
		fclose(f);
	} else {
		rs = 1;
	}
	stopReader();
	return rs;
}
