#define __STDC_LIMIT_MACROS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <time.h>
#include <unistd.h>
#include "CVAPIV01_DESFire.h"
#include "carto.h"
#include "pass.h"
#include "breader.h"
#include "iface.h"
#include "confreader.h"
#include "errcode.h"

#define MIN_DURATION 40

int main(int argc, char * const *argv)
{
	init_configuration(argc, argv, "v:V:");
	int rs = ERR_USAGE;
	if(has_value2add || has_value2set) {
		srand(time(NULL));
		rs = openSerial(serial_type, device_index);
		if(rs == 0) {
			uint64_t uid = card_uid;
			rs =tr_select(&uid);
			if(rs == 0) {
				ifprintf("UID=%.16llX", uid);
				DataBundle bundle;
				int passindex;
				rs = selected_read_carto(passlist, passcount, bundle);
				if(rs == 0) {
					if(bundle.mem.carto.d.v1.uid == uid) {
						TotalBlock tb;
						rs = selected_read_total(passlist[bundle.passindex], tb);
						if(rs == 0) {
							if(has_value2set) {
								if(value2set >= INT16_MIN && value2set <= INT16_MAX) {
									tb.d.v.val = value2set;
								} else {
									iferrorf("Value (%ld) can not be bigger than %d and smaller than %d", value2set, INT16_MAX, INT16_MIN);
									rs = ERR_VAL_LIMIT;
								}
							}
							if(has_value2add) {
								long totval = tb.d.v.val + value2add;
								if(totval >= INT16_MIN && totval <= INT16_MAX) {
									tb.d.v.val = totval;
								} else {
									iferrorf("Total value (%ld) can not be bigger than %d and smaller than %d", totval, INT16_MAX, INT16_MIN);
									rs = ERR_VAL_LIMIT;
								}
							}
							if(tb.d.v.val > bundle.mem.carto.d.v1.maxval) {
								//let's make maximum value == current value
								bundle.mem.carto.d.v1.maxval = tb.d.v.val;
								rs = selected_write_carto(passlist[bundle.passindex], bundle);
							}
							if(rs == 0) {
								rs = selected_write_total(passlist[bundle.passindex], tb);
								if(rs == 0) {
									TotalBlock tbr;
									rs = selected_read_total(passlist[bundle.passindex], tbr);
									if(rs == 0) {
										printf("Written value=%d\n",tbr.d.v.val);
										ifprintf("VALUE=%d",tbr.d.v.val);
									} else {
										printf("Can NOT read current value !!!\n");
									}
								} else {
									printf("Can NOT write current value !!!\n");
								}
							}
						}
					} else {
						printf("Fake card\n");
						rs = ERR_FAKE_CARD;
					}
				}
				tr_unselect();
			}
		} else {
			printf("Open serial port is unsuccessfull\n");
		}
	} else {
		printf("Usage: addvalue [-v <value to add>] [-V <value to set>]\n");
	}
	do_exit(rs);
}
