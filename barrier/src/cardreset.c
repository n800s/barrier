#define __STDC_LIMIT_MACROS

#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include <memory.h>
#include <time.h>
#include <unistd.h>
#include "CVAPIV01_DESFire.h"
#include "carto.h"
#include "pass.h"
#include "breader.h"
#include "confreader.h"
#include "debug.h"
#include "iface.h"
#include "errcode.h"

int main(int argc, char * const *argv)
{
	init_configuration(argc, argv, "t:p:V:b:e:");
	int rs;
	if(value2set < INT16_MIN || value2set > INT16_MAX) {
		iferrorf("Value (%ld) can not be bigger than %d and smaller than %d", value2set, INT16_MAX, INT16_MIN);
		rs = ERR_VAL_LIMIT;
	} else {
		long val;
		rs = openSerial(serial_type, device_index);
		if(rs == 0) {
			CARD_TYPE ct;
			if(strcmp(ctype_str, "child") == 0) {
				ct = CT_CHILD;
			} else if(strcmp(ctype_str, "adult") == 0) {
				ct = CT_ADULT;
			} else {
				iferrorf("Type '%s' is wrong\n", ctype_str);
				ctype_str[0] = 0;
				rs = ERR_CTYPE;
			}
			CARD_SUBTYPE cst;
			if(strcmp(csubtype_str, "periodonly") == 0) {
				cst = CST_PERIODONLY;
			} else if (csubtype_str[0]) {
				cst = CST_NONE;
				iferrorf("Subtype '%s' is wrong\n", csubtype_str);
				csubtype_str[0] = 0;
				rs = ERR_CSUBTYPE;
			}
			if(rs == 0) {
				uint64_t uid = card_uid;
				rs=tr_init_v1(uid, rand_pass(), ct, cst, val=value2set, tbegin=tbegin, tend=tend);
				printf("init ret=%d\n", rs);
				if(rs == 0) {
					int rs = tr_select(&uid);
					if(rs == 0) {
						DataBundle bundle;
						TotalBlock tb;
						rs = selected_read_carto(passlist, passcount, bundle);
						printf("selected_read_carto ret=%d\n", rs);
						if(rs == 0) {
							ifprintf("UID=%.16llX", bundle.mem.carto.d.v1.uid);
							ifprintf("MAXVALUE=%d", bundle.mem.carto.d.v1.maxval);
							ifprintf("STARTTIME=%s", time2str(bundle.mem.carto.d.v1.tbegin));
							ifprintf("ENDTIME=%s", time2str(bundle.mem.carto.d.v1.tend));
							ifprintf("TYPE=%s", ctype_str);
							ifprintf("SUBTYPE=%s", csubtype_str);
							rs = selected_read_total(passlist[bundle.passindex], tb);
							if(rs == 0) {
								ifprintf("VALUE=%d", tb.d.v.val);
							}
						}
					}
					tr_unselect();
				}
			}
		} else {
			printf("Open serial port is unsuccessfull\n");
		}
	}
	do_exit(rs);
}
