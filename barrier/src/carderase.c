#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include <memory.h>
#include <time.h>
#include <unistd.h>
#include "CVAPIV01_DESFire.h"
#include "carto.h"
#include "pass.h"
#include "breader.h"
#include "confreader.h"
#include "debug.h"

int main(int argc, char *argv[])
{
	init_configuration(argc, argv, "q");
	int rs;
	char sbuf[MAX_BLOCK_COUNT*4+200];
	unsigned char block[BLOCK_SIZE];
	rs = openSerial(serial_type, device_index);
	if(rs == 0) {
		if( !quietmode ) {
			char version[256];
			printf("getVersion returns:%d\n", GetVersionAPI(version));
			printf("version=%s\n", version);
			sbuf[0] = 0;
			printf("GetUserInfo returns:%d\n", GetUserInfo(reader_address, sbuf));
			printf("user info: %s\n", sbuf);
			version[0] = 0;
			printf("getVersionNum returns:%d\n", GetVersionNum(reader_address, version));
		    printf("version=%s\n", version);
		}
		uint64_t uid = card_uid;
		rs = tr_select(&uid);
		if(rs == 0) {
			for(int i=0; i< 100 && rs != 27; i++) {
				memset(block, 0, sizeof(block));
				rs = nonzeromsg(CMD_ISO15693_Write(reader_address, 0x10, i, 1, NULL, block), "CMD_ISO15693_Write");
				if(!quietmode) {
					printf("block %d: %d\n", i, rs);
				}
			}
			if(rs == 27) {
				rs = 0;
			}
			tr_unselect();
		}
	} else {
		printf("Open serial port is unsuccessfull\n");
	}
	do_exit(rs);
}
