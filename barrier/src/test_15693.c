#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include "CVAPIV01_DESFire.h"

int main(int argc, char * const *argv)
{
	if(argc < 4) {
		printf("Usage: test_reader <communication type i.e. 0 or 1> <device 0,1 or 2> <device index 0,1,...>\n");
	} else {
		int type = atoi(argv[1]);
		int dev = atoi(argv[2]);
		char *port = argv[3];
		int rs;
		CV_SetCommunicationType(type);//CVReader function, use serial
#ifdef TION_PRO28
		fprintf(stderr, "OpenComm returns %d\n", rs = OpenComm(port, 115200));
#else
		fprintf(stderr, "OpenComm returns %d\n", rs = OpenComm(port, 115200, dev));
#endif
		if(rs == 0) {
			unsigned char buf[50];
			unsigned char mask[8];
			memset(mask, 0xff, sizeof(mask));
			fprintf(stderr, "CMD_ISO15693_Inventory returns %d\n", rs = CMD_ISO15693_Inventory(0, 0, 0, 0, mask, buf));
			if(rs == 0) {
				uint64_t *puid = (uint64_t *)buf;
				printf("UID=%.16llX\n", *puid);
			}
			CloseComm();
		}
	}
	return 0;
}
