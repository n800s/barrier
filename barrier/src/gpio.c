#include "gpio.h"

#include <stdio.h>

#ifdef MOXA
#warning MOXA GPIO
#include <moxadevice.h>
#elif TION_PRO28
#warning TION PRO28 GPIO
#endif



void set_gpio_value(int index, int value)
{
#ifdef MOXA
	set_dout_state(index, value);
#elif TION_PRO28
/*
0 = 53 -> X22, 10
1 = 54 -> X22, 11
2 = 55 -> X22, 12
3 = 56 -> X22, 15
*/
	if(index < 53) {
		index += 53;
	}
	if(index >= 53 && index <= 56) {
		char pbuf[256];
		FILE *f;
		f = fopen("/sys/class/gpio/export", "w");
		fprintf(f, "%d", index);
		fclose(f);
		sprintf(pbuf, "/sys/class/gpio/gpio%d/direction", index);
		f = fopen(pbuf, "w");
		fputs("out", f);
		fclose(f);
		sprintf(pbuf, "/sys/class/gpio/gpio%d/value", index);
		f = fopen(pbuf, "w");
		fprintf(f, "%d", value);
		fclose(f);
		f = fopen("/sys/class/gpio/unexport", "w");
		fprintf(f, "%d", index);
		fclose(f);
	}
#endif
}

