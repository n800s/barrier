#include "barrier.h"
#include "breader.h"
#include "debug.h"
#include "pass.h"
#include "CVAPIV01_DESFire.h"
#include "confreader.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <pwd.h>
#include <signal.h>
#include "gpio.h"
#include "errcode.h"

#define DAEMONUSER "reader"

#define ACT_MAX_N 50
#define NUMS_MAX_N 10
#define PATTERN_SIZE 5

volatile sig_atomic_t got_usr1;

void sigusr1_handler(int sig)
{
	got_usr1 = 1;
}

void execute_event(const char event[])
{
	int n_act = 0;
	char *eventbuf = strdup(event);
	char *pact[ACT_MAX_N];
	dbprintf("Starting event %s\n", event);
	pact[n_act] = strtok(eventbuf, ":");
	while (pact[n_act] != NULL && n_act < ACT_MAX_N)
	{
		pact[++n_act] = strtok(NULL, ":");
	}
	for(int i=0; i<n_act; i++) {
		char act = pact[i][0];
		char *actbuf = strdup(pact[i] + 1);
		int n_nums = 0;
		char *pnums[NUMS_MAX_N];
		pnums[n_nums] = strtok(actbuf, ",");
		while (pnums[n_nums] != NULL && n_nums < NUMS_MAX_N)
		{
			pnums[++n_nums] = strtok(NULL, ",");
		}
		long nums[NUMS_MAX_N];
		dbprintf("act=%c", act);
		for(int j=0; j<n_nums; j++) {
			nums[j] = atol(pnums[j]);
			dbprintf(" %d ",nums[j]);
		}
		dbprintf("\n");
		unsigned char pattern[PATTERN_SIZE] = {1, 1, 1, 1, 1}, patternbuf[PATTERN_SIZE];
		switch(act) {
			case 'R':
				if(n_nums >= 2) {
					ActiveLED(reader_address, 1, nums[0], nums[1]);
				}
				break;
			case 'G':
				if(n_nums >= 2) {
					ActiveLED(reader_address, 2, nums[0], nums[1]);
				}
				break;
			case 'B':
				if(n_nums >= 2) {
					ActiveLED(reader_address, 3, nums[0], nums[1]);
				}
				break;
			case 'Z':
				for(int k=0; k<PATTERN_SIZE; k++) {
					patternbuf[k] = (k<n_nums) ? nums[k] : pattern[k];
				}
				ActiveBuzzer(reader_address, 4, pattern);
				break;
			case 'P':
				if(n_nums > 0) {
					usleep(nums[0]);
				} else {
					usleep(1000000);
				}
				break;
			case 'H':
				if(n_nums > 0) {
#ifdef MOXA
					set_gpio_value(nums[0], 1);
#endif
				}
				break;
			case 'L':
				if(n_nums > 0) {
#ifdef MOXA
					set_gpio_value(nums[0], 0);
#endif
				}
				break;
		}
		free(actbuf);
	}
	dbprintf("End of event\n");
	free(eventbuf);
	return;
}

bool do_rollover()
{
	bool rs = false;
	static int last_hour = -1;
	time_t curtime = time(NULL);
	struct tm *tm_struct = localtime(&curtime);
	if(last_hour < 0) {
		last_hour = tm_struct->tm_hour;
	} else if(everyhour_rollover_mode) {
		if(last_hour != tm_struct->tm_hour) {
			rs = true;
			last_hour = tm_struct->tm_hour;
		}
	}
	return rs;
}

void barrier_loop()
{
	int rs;
	static uint64_t last_uid=0;
	static int last_time=0;
	bool conf_error = update_config(configfile);
	if(!conf_error) {
		if( adult_price == 0 ) {
			dbprintf("Adult price is zero\n");
			conf_error = true;
		}
		if( child_price == 0 ) {
			dbprintf("Child price is zero\n");
			conf_error = true;
		}
	}
	if(conf_error) {
		printf("Error reading config file\n");
		rs = ERR_USAGE;
	} else while(true) {
		if(got_usr1 || do_rollover()) {
			dbprintf("Starting rollover...\n");
			FILE *f;
			got_usr1 = 0;
			f = fopen_split(errfile, "a", max_error_size, NULL, true);
			if(!f) {
				fclose(f);
			}
			f = fopen_split(dbfile, "a", max_transact_size, NULL, true);
			if(!f) {
				fclose(f);
			}
			dblog_rollover(true);
			dbprintf("Rollover is over\n");
		}
		uint64_t uid = 0;
		SetLED(reader_address, 2);
		rs =tr_select(&uid);
		if(rs == 0) {
			SetLED(reader_address, 0);
			int curtime = time(NULL);
			if(uid != last_uid || (curtime - last_time) > d_timeout) {
				DataBundle bundle;
				rs = tr_bundle_read(uid, passlist, passcount, bundle);
				if(rs == 0) {
					CARD_TYPE ctype = (CARD_TYPE)bundle.mem.carto.d.v1.ctype;
					CARD_SUBTYPE csubtype = (CARD_SUBTYPE)bundle.mem.carto.d.v1.csubtype;
					int cost = 0;
					bool in_period = (bundle.mem.carto.d.v1.tbegin == 0 && bundle.mem.carto.d.v1.tend == 0 && csubtype != CST_PERIODONLY) || (bundle.mem.carto.d.v1.tbegin <= curtime && bundle.mem.carto.d.v1.tend >= curtime);
					time_t curtime = time(NULL);
					switch(ctype) {
						case CT_CHILD:
							dbprintf("Child card type\n");
							cost = child_price;
							break;
						case CT_ADULT:
							dbprintf("Adult card type\n");
							cost = adult_price;
							break;
						default:
							dbprintf("Unknown card type %d\n", ctype);
							break;
					}
					switch(csubtype) {
						case CST_PERIODONLY:
							dbprintf("Period only card subtype\n");
							break;
					}
					FILE *f = fopen_split(dbfile, "a", max_transact_size, "CURTIME,UID,TYPE,SUBTYPE,MAXVAL,COST,BOND,PBEGIN,PEND,INPERIOD,REMAINS,\n");
					if(!f) {
						dbprintf("Failing opening file %s\n", dbfile);
					}
					fprintf(f, "%d,\"%.16llX\",%d,%d,%d,%d,%d,%d,%d,%d,", (int)curtime, bundle.mem.carto.d.v1.uid,
						bundle.mem.carto.d.v1.ctype, bundle.mem.carto.d.v1.csubtype, bundle.mem.carto.d.v1.maxval,
						-cost, bond_value, bundle.mem.carto.d.v1.tbegin, bundle.mem.carto.d.v1.tend, in_period);
					fflush(f);
					int passcode = 0;
					if(csubtype == CST_PERIODONLY) {
						if(in_period) {
							passcode = 1;
							dbprintf("In period\n");
							execute_event(events[(ctype==CT_ADULT) ? ADULT_TIME_CHECK : CHILD_TIME_CHECK]);
						} else {
							dbprintf("Not in period\n");
							execute_event(events[ERROR_EVENT]);
						}
						fprintf(f, ",");
					} else	if( cost > 0 ) {
						TotalBlock tb;
						rs = selected_read_total(passlist[bundle.passindex], tb);
						if(rs == 0) {
							int val = tb.d.v.val;
							if(!in_period) {
								dbprintf("Not in period\n");
								execute_event(events[ERROR_EVENT]);
							} else if(tb.d.v.val < (cost + bond_value)) {
								dbprintf("Found %d NOT ENOUGH\n", tb.d.v.val);
								execute_event(events[ERROR_EVENT]);
							} else {
								passcode = 1;
								dbprintf("Found %d\n", tb.d.v.val);
								val = tb.d.v.val -= cost;
								dbprintf("writing %d\n", tb.d.v.val);
								rs = selected_write_total(passlist[bundle.passindex], tb);
								if(rs == 0) {
									dbprintf("written\n");
									switch(ctype) {
										case CT_CHILD:
											execute_event(events[CHILD_PAYMENT]);
											break;
										default:
											execute_event(events[ADULT_PAYMENT]);
											break;
									}
								} else {
									dbprintf("Error writing current total %d\n", rs);
									execute_event(events[ERROR_EVENT]);
								}
							}
							fprintf(f, "%d,", val);
						}
					} else {
						fprintf(f, ",");
					}
					fprintf(f, "%d\n", passcode);
					fclose(f);
					if( rs == 0 ) {
						last_uid = uid;
						last_time = curtime;
					}
				} else {
					FILE *f = fopen_split(errfile, "a", max_error_size);
					if(!f) {
						dbprintf("Failing opening error log file %s\n", errfile);
					}
					fprintf(f, "%d,\"%.16llX\",%d\n", curtime, bundle.mem.carto.d.v1.uid, rs);
					fclose(f);
				}
			} else {
				dbprintf("Doubling (%d seconds remaining)\n", d_timeout - (curtime - last_time));
			}
			tr_unselect();
		} else {
			//dbprintf("Not found\n");
		}
	}
}

void check_add_nl(const char *fname)
{
	//check for the last symbol in a file and if it is not \n then write \n
	FILE *f = fopen(fname, "r+");
	if(f) {
		fseek(f, -1, SEEK_END);
		int lastc = fgetc(f);
		if ( lastc != 0xA ) {
			fseek(f, 0, SEEK_END);
			const char *nl = "\n";
			int n = fwrite(nl, 1, strlen(nl), f);
			dbprintf("An end of line added to %s file\n", fname);
		}
		fclose(f);
	}
}

int main(int argc, char *const *argv)
{
	struct passwd *pw = getpwnam(DAEMONUSER);
	if(!pw) {
		printf("User %s not found\n", DAEMONUSER);
		do_exit(EXIT_FAILURE);
	} else {
		setuid(pw->pw_uid);

		struct sigaction sa;

		got_usr1 = 0;

		sa.sa_handler = sigusr1_handler;
		sa.sa_flags = 0;
		sigemptyset(&sa.sa_mask);

		if (sigaction(SIGUSR1, &sa, NULL) == -1) {
			perror("sigaction");
			exit(1);
		}

		init_configuration(argc, argv, "l:");

		dbprintf("Starting...\n");

		int rs = openSerial(serial_type, device_index);
		if(rs == 0) {
			check_add_nl(dbfile);
			barrier_loop();
		}
		do_exit(EXIT_SUCCESS);
	}
}

