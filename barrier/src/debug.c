#include "debug.h"
#include "confreader.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <glob.h>

bool debugmode=0;

bool quietmode = false;

FILE *flog=NULL;
char log_filename[256];

FILE *log_open(const char *logfile)
{
	if(max_log_count > 0) {
		char pattern[300];
		glob_t globbuf;
		snprintf(pattern, sizeof(pattern), "%s.*.arc", logfile);
		glob(pattern, 0, NULL, &globbuf);
		for(int i=0; i+max_log_count < globbuf.gl_pathc+1; i++) {
			remove(globbuf.gl_pathv[i]);
		}
		globfree(&globbuf);
	}
	strncpy(log_filename, logfile, sizeof(log_filename));
	flog = fopen_split(logfile, "a", max_log_size);
	return flog;
}

void log_close()
{
	if(flog) {
		fclose(flog);
	}
}

void dbprintf(const char *format, ...)
{
	static bool add_date = true;
	va_list va;
	va_start(va, format);
	if(debugmode) {
		vfprintf(stderr, format, va);
	}
	if(flog) {
		if(add_date) {
			time_t t = time(NULL);
			struct tm *ti=localtime(&t);
			fprintf(flog, "%.4d-%.2d-%.2d %.2d:%.2d:%.2d: ", ti->tm_year + 1900, ti->tm_mon + 1, ti->tm_mday, ti->tm_hour, ti->tm_min, ti->tm_sec);
			add_date = false;
		}
		vfprintf(flog, format, va);
		if(strstr(format, "\n")) {
			add_date = true;
		}
		fflush(flog);
		dblog_rollover();
	}
	va_end(va);
}

int nonzeromsg(int rs, const char funcname[])
{
	if(rs) {
		dbprintf("%s returns %d\n", funcname, rs);
	}
	return rs;
}

void dumpbuf(const unsigned char *buf, int buflen)
{
	if(debugmode) {
		for(int i=0; i< buflen; i+=16) {
			for(int j=0; j< 16; j++) {
				if(i+j >= buflen)
					break;
				dbprintf("%.2X ", buf[j+i]);
			}
			dbprintf("\n");
		}
	}
}

void dblog_rollover(bool force)
{
	long fsize = ftell(flog);
	if((max_log_size > 0 && fsize > max_log_size) || (force && fsize > 0) ) {
		//rollover
		log_close();
		log_open(log_filename);
	}
}
