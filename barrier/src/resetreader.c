#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "CVAPIV01_DESFire.h"
#include "confreader.h"
#include "breader.h"
#include "debug.h"
#include "errcode.h"

int main(int argc, char * const *argv)
{
	init_configuration(argc, argv, "a:");
	int rs = ERR_USAGE;
	rs = openSerial(serial_type, device_index);
	if(rs == 0) {
		rs = nonzeromsg(ReaderReset(reader_address), "ReaderReset");
	} else {
		printf("Open reader is unsuccessfull\n");
	}
	do_exit(rs);
	dbprintf("exiting\n");
}
