#include <stdio.h>
#include <memory.h>
#include <unistd.h>
#include "carto.h"
#include "breader.h"
#include "confreader.h"
#include "debug.h"
#include "CVAPIV01_DESFire.h"
#include <time.h>
#include <pthread.h>
#include "errcode.h"

int openUSB()
{
	CV_SetCommunicationType(1);//CVReader function, use USB
	return 0;
}

int openSerial(int type, int index)
{
	int rs;
	if(usb_mode) {
		rs = openUSB();
	} else {
		char port[10];
		snprintf(port, sizeof(port), "COM%d", index);
		CV_SetCommunicationType(0);//CVReader function, use serial
		for(int i =0; i<20; i++) {
			if(i > 0) {
				dbprintf("Serial port connection failed %d\n. Trying again...", rs);
			}
#ifdef TION_PRO28
	  		rs = OpenComm(port, 115200);
#else
	  		rs = OpenComm(port, 115200, type);
#endif
			if(rs == 0) {
				break;
			}
			sleep(1);
		}
	}
	return rs;
}

void stopReader()
{
	if(!usb_mode) {
		CloseComm();
		pthread_exit(NULL);
	}
}

int tr_select(uint64_t *puid, int retry)
{
	unsigned char buf[50];
	unsigned char mask[8];
	int rs = 0;
	memset(mask, 0xff, sizeof(mask));
	if(puid && *puid) {
		dbprintf("Requested card %.16llX\n", *puid);
		*(uint64_t*)buf = *puid;
	} else {
		rs = nonzeromsg(CMD_ISO15693_Inventory(reader_address, 0, 0, 0, mask, buf), "CMD_ISO15693_Inventory");
		if(rs == 0) {
			dbprintf("Inventory uid:%.16llX\n", *(uint64_t *)buf);
		}
	}
	if(rs == 0) {
		sleep(1);
		rs = nonzeromsg(CMD_ISO15693_Select(reader_address, 0x20, buf), "CMD_ISO15693_Select");
		if(rs == 0 && puid) {
			*puid = *(uint64_t*)buf;
		}
	}
/*	if(retry && rs == 5) {
		dbprintf("Retrying\n");
		nonzeromsg(CMD_ISO15693_Select(reader_address, 0, buf), "CMD_ISO15693_Select");
		nonzeromsg(CMD_ISO15693_Reset_To_Ready(reader_address, 0x04, buf), "CMD_ISO15693_Reset_To_Ready");
		nonzeromsg(ReaderReset(reader_address), "ReaderReset");
		sleep(5);
		rs = tr_select(puid, 0);
	}
*/	return rs;
}

int tr_unselect()
{
	return nonzeromsg(CMD_ISO15693_Reset_To_Ready(reader_address, 0x10, NULL), "CMD_ISO15693_Reset_To_Ready");
}

int tr_init_v1(uint64_t &uid, const char key[], CARD_TYPE ctype, CARD_SUBTYPE csubtype, long value, time_t tbegin, time_t tend)
{
	int rs = tr_select(&uid);
	if(rs == 0) {
		DataBundle bundle;
		TotalBlock tb;
		bundle.mem.carto.v = 1;
		bundle.mem.carto.d.v1.uid = uid;
		bundle.mem.carto.d.v1.ctype = ctype;
		bundle.mem.carto.d.v1.csubtype = csubtype;
		bundle.mem.carto.d.v1.maxval = value;
		bundle.mem.carto.d.v1.tbegin = tbegin;
		bundle.mem.carto.d.v1.tend = tend;
		tb.d.v.val = value;
		rs = selected_write_carto(key, bundle);
		if(rs == 0) {
			rs = selected_write_total(key, tb);
			TotalBlock tb1;
			selected_read_total(key, tb1);
		}
		tr_unselect();
	}
	return rs;
}

int tr_bundle_read(const uint64_t &uid, const char *passes[], int passcount, DataBundle &bundle)
{
	int rs;
	rs = selected_read_carto(passes, passcount, bundle);
	if(rs == 0) {
		if(bundle.mem.carto.d.v1.uid != uid) {
			printf("Fake card\n");
			rs = ERR_FAKE_CARD;
		}
	}
	return rs;
}
