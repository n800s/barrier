#include	<stdio.h>
#include	<stdlib.h>
#include "pass.h"

const char *passlist[] = {
	"JSQZwMxVeWPNg0Kum4y1iHE2qlhDcdFTXO",
	"OxzQonj1WcJCBruvIG89KXNhFH0dSy64Yw",
	"TWlKCJnBQdzDcEUtN5j4g3ZA7RpG0HVYP6",
	"cuftlPSy6gj1YbqFTs2LNVADmokJ94aRMn",
	"ne84bnqRinad84ntGHV,sdflqedFrewdew"
};

const int passcount = sizeof(passlist)/sizeof(*passlist);

const char *rand_pass(int old_passindex)
{
	return passlist[rand_passindex(old_passindex)];
}

int rand_passindex(int old_passindex)
{
	int rs = 0;
	if(passcount > 1) {
		do {
			rs = rand() % passcount;
		} while(rs == old_passindex);
	} else {
		rs = 0;
	}
	return rs;
}
