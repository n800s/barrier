#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/des.h>
 
 
void DESEncrypt( const unsigned char *Key, unsigned char *Msg, int size)
{
 
        int             n=0;
        DES_cblock      Key2;
        DES_key_schedule schedule;
        unsigned char*    Res = ( unsigned char * ) malloc( size );
 
        /* Prepare the key for use with DES_cfb64_encrypt */
        memcpy( Key2, Key,8);
        DES_set_odd_parity( &Key2 );
        DES_set_key_checked( &Key2, &schedule );
 
        /* Encryption occurs here */
        DES_cfb64_encrypt( Msg, Res, size, &schedule, &Key2, &n, DES_ENCRYPT );

		memcpy(Msg, Res, size); 

		free(Res);
}
 
 
void DESDecrypt( const unsigned char *Key, unsigned char *Msg, int size)
{
 
        int             n=0;
 
        DES_cblock      Key2;
        DES_key_schedule schedule;
        unsigned char*    Res = ( unsigned char * ) malloc( size );
 
        /* Prepare the key for use with DES_cfb64_encrypt */
        memcpy( Key2, Key,8);
        DES_set_odd_parity( &Key2 );
        DES_set_key_checked( &Key2, &schedule );
 
        /* Decryption occurs here */
        DES_cfb64_encrypt( Msg, Res, size, &schedule, &Key2, &n, DES_DECRYPT );
 
		memcpy(Msg, Res, size); 

		free(Res);
}
