#include "informer.h"
#include "breader.h"
#include "debug.h"
#include "iface.h"
#include "pass.h"
#include "CVAPIV01_DESFire.h"
#include "confreader.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pwd.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include "errcode.h"

#define DAEMONUSER "reader"

void informer_loop()
{
	int rs;
	static uint64_t last_uid=0;
	static int last_time=0;
	printf("Hello\n");
	bool conf_error = update_config(configfile);
	if(conf_error) {
		fprintf(stderr, "Error reading config file\n");
		rs = ERR_USAGE;
	} else while(true) {
		uint64_t uid = 0;
		SetLED(reader_address, 2);
		rs =tr_select(&uid);
		if(rs == 0) {
			SetLED(reader_address, 0);
			int curtime = time(NULL);
			if(uid != last_uid || (curtime - last_time) > d_timeout) {
				DataBundle bundle;
				rs = tr_bundle_read(uid, passlist, passcount, bundle);
				if(rs == 0) {
					dbprintf("Found UID=%.16llX\n", bundle.mem.carto.d.v1.uid);
					CARD_TYPE ctype = (CARD_TYPE)bundle.mem.carto.d.v1.ctype;
					CARD_SUBTYPE csubtype = (CARD_SUBTYPE)bundle.mem.carto.d.v1.csubtype;
					const char *sctype;
					switch(bundle.mem.carto.d.v1.ctype) {
						case CT_ADULT:
							sctype = "adult";
							break;
						case CT_CHILD:
							sctype = "child";
							break;
						default:
							sctype = "unknown";
							break;
					}
					const char *scsubtype;
					switch(bundle.mem.carto.d.v1.csubtype) {
						case CST_PERIODONLY:
							scsubtype = "periodonly";
							break;
						default:
							scsubtype = "";
							break;
					}
					int passcode = 0;
					TotalBlock tb;
					rs = selected_read_total(passlist[bundle.passindex], tb);
					if(rs == 0) {
						ifprintf("BLOCK=BEGIN");
						ifprintf("UID=%.16llX", bundle.mem.carto.d.v1.uid);
						ifprintf("STARTTIME=%d", bundle.mem.carto.d.v1.tbegin);
						ifprintf("ENDTIME=%d", bundle.mem.carto.d.v1.tend);
						ifprintf("TYPE=%s", sctype);
						ifprintf("SUBTYPE=%s", scsubtype);
						ifprintf("VALUE=%d", tb.d.v.val);
						ifprintf("BLOCK=END");
					}
				}
				last_uid = uid;
				last_time = curtime;
			} else {
				dbprintf("Doubling (%d seconds remaining)\n", d_timeout - (curtime - last_time));
			}
			tr_unselect();
		} else {
			//dbprintf("Not found\n");
		}
	}
}

int main(int argc, char *const *argv)
{
	struct passwd *pw = getpwnam(DAEMONUSER);
	if(!pw) {
		printf("User %s not found\n", DAEMONUSER);
		do_exit(EXIT_FAILURE);
	} else {
		setuid(pw->pw_uid);

		init_configuration(argc, argv, "l:");

		dbprintf("Starting...\n");

		int rs = openSerial(serial_type, device_index);
		if(rs == 0) {
			informer_loop();
		}
		do_exit(EXIT_SUCCESS);
	}
}

