#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/param.h>
#include "carto.h"
#include "crc16.h"
#include "debug.h"
#include "CVAPIV01_DESFire.h"
#include <openssl/aes.h>
#include "aescrypt.h"
#include "descrypt.h"
#include "confreader.h"
#include "errcode.h"

//check for maximum size of CARTO that can not be bigger than MAX_BLOCK_COUNT*BLOCK_SIZE
char size_test[MAX_BLOCK_COUNT*BLOCK_SIZE - sizeof(CARTO)];
//check for number of blocks divided by AES_BLOCK_SIZE without remaining
char size_test1[MAX_BLOCK_COUNT*BLOCK_SIZE / AES_BLOCK_SIZE];
//the array is not used in the code so far

uint16_t DataBundle::calc_crc()
{
	dbprintf("calculating crc for\n");
	dumpbuf(mem.buf, sizeof(mem.buf));
	uint16_t rs = crc16(mem.buf, sizeof(mem.buf));
	dbprintf("calculated crc=%.4X\n", rs);
	return rs;
}

#define P_FLAGS 0x12

void bf_encrypt(const char *key, CARTO_MEM &m)
{
	process(true, (const uint8_t*)key, strlen(key), m.buf, sizeof(m.buf));
}

void bf_decrypt(const char *key, CARTO_MEM &m)
{
	process(false, (const uint8_t*)key, strlen(key), m.buf, sizeof(m.buf));
}

void bf_encrypt(const char *key, TotalBlock &m)
{
	DESEncrypt((const uint8_t*)key, m.d.buf, sizeof(m.d.buf));
}

void bf_decrypt(const char *key, TotalBlock &m)
{
	DESDecrypt((const uint8_t*)key, m.d.buf, sizeof(m.d.buf));
}

int selected_read_firstblock(FirstBlock &fb, int reader_address)
{
	int rs;
	unsigned char buf[BLOCK_SIZE];
	rs = nonzeromsg(CMD_ISO15693_Read(reader_address, P_FLAGS, FIRST_BLOCK_OFFSET, 1, NULL, buf), "CMD_ISO15693_Read");
	if (rs == 0) {
		fb.loc = buf[0] % 2;
		fb.crc = *(uint16_t*)(buf+1);
	}
	dbprintf("CRC read=%.4X\n", fb.crc);
	return rs;
}

int selected_read_carto(const char *passes[], int passcount, DataBundle &db)
{
	int rs;
	FirstBlock fb;
	rs = selected_read_firstblock(fb, reader_address);
//	dbprintf("read first block ret=%d\n", rs);
	if (rs == 0) {
		dbprintf("location=%ld\n", fb.loc);
		int startblock = (fb.loc) ? BUNDLE_OFFSET + MAX_BLOCK_COUNT : BUNDLE_OFFSET;
		dbprintf("read startblock:%d\n", startblock);
		rs = nonzeromsg(CMD_ISO15693_Read(reader_address, P_FLAGS, startblock, MAX_BLOCK_COUNT, NULL, db.mem.buf), "CMD_ISO15693_Read");
		if (rs == 0) {
			db.passindex = -1;
			for(int i=0; i<passcount; i++) {
				db.backup();
				bf_decrypt(passes[i], db.mem);
				dbprintf("Read time %d:\n", i);
				dumpbuf(db.mem.buf, sizeof(db.mem.buf));
				if(db.calc_crc() != fb.crc) {
					rs = ERR_CRC;
					db.restore();
				} else {
					rs = 0;
					db.passindex = i;
					break;
				}
			}
		}
		dbprintf("read carto ret=%d\n", rs);
	}
	return rs;
}

int selected_write_firstblock(FirstBlock &fb, int reader_address)
{
	int rs;
	unsigned char buf[BLOCK_SIZE];
	memcpy(buf, &fb, MIN(sizeof(buf), sizeof(fb)));
	dbprintf("CRC write=%.4X\n", fb.crc);
	srand(time(NULL));
	for(int i=sizeof(fb); i<sizeof(buf); i++) {
		buf[i] = rand() % 256;
	}
	buf[0] = (buf[0]) ? (rand() % 256 | 1) : (rand() % 256 & 0xfe);
	rs = nonzeromsg(CMD_ISO15693_Write(reader_address, P_FLAGS, FIRST_BLOCK_OFFSET, 1, NULL, buf), "CMD_ISO15693_Write");
	return rs;
}

//it works only BLOCK2WRITE == 1
#define BLOCK2WRITE 1

int selected_write_carto(const char key[], DataBundle &db)
{
	int rs;
	FirstBlock fb;
	rs = selected_read_firstblock(fb, reader_address);
//	dbprintf("read first block ret=%d\n", rs);
	if (rs == 0) {
		fb.loc = !fb.loc;
		dbprintf("location=%ld\n", fb.loc);
		int startblock = (fb.loc) ? BUNDLE_OFFSET + MAX_BLOCK_COUNT : BUNDLE_OFFSET;
		fb.crc = db.calc_crc();
		dbprintf("Write:\n");
		dumpbuf(db.mem.buf, sizeof(db.mem.buf));
		dbprintf("write to startblock:%d\n", startblock);
		bf_encrypt(key, db.mem);
		time_t tb = time(NULL);
		for(int i=0; i<MAX_BLOCK_COUNT; i+=BLOCK2WRITE) {
			int iw = 1;
			do {
				rs = nonzeromsg(CMD_ISO15693_Write(reader_address, P_FLAGS, startblock+i, BLOCK2WRITE, NULL, db.mem.blocks[i]), "CMD_ISO15693_Write");
				dbprintf("%d:%d write data ret=%d\n", startblock+i, iw, rs);
				iw++;
				if(iw > 10 || rs==4 || rs==27)
					break;
			} while(rs);
		}
//		printf("time delta 1: %d\n", time(NULL) - tb);
		if (rs == 0) {
			rs = selected_write_firstblock(fb, reader_address);
			dbprintf("write first block ret=%d\n", rs);
		}
//		printf("time delta 2: %d\n", time(NULL) - tb);
		if(rs == 0) {
			//write garbage to another location
			startblock = (fb.loc) ? BUNDLE_OFFSET : BUNDLE_OFFSET + MAX_BLOCK_COUNT;
			uint8_t block[BLOCK_SIZE];
			for(int i=0; i<MAX_BLOCK_COUNT; i++) {
				for(int j=0; j<BLOCK_SIZE; j++) {
					block[j] = rand() % 256;
				}
				dbprintf("write garbage to block %d ret=%d\n", startblock+i, 
					nonzeromsg(CMD_ISO15693_Write(reader_address, P_FLAGS, startblock+i, 1, NULL, block), "CMD_ISO15693_Write"));
			}
//			printf("time delta 3: %d\n", time(NULL) - tb);
			//write garbage to remaining blocks
			int ret, i = BUNDLE_OFFSET + 2 * MAX_BLOCK_COUNT;
			do {
				for(int j=0; j<BLOCK_SIZE; j++) {
					block[j] = rand() % 256;
				}
				ret = nonzeromsg(CMD_ISO15693_Write(reader_address, P_FLAGS, i, 1, NULL, block), "CMD_ISO15693_Write");
				dbprintf("write garbage to block %d ret=%d\n", i, ret);
				i++;
			} while(ret == 0);
//			printf("time delta 4: %d\n", time(NULL) - tb);
		}
		dbprintf("write carto ret=%d\n", rs);
	}
	return rs;
}

int selected_read_total(const char key[], TotalBlock &tb)
{
	int rs;
	rs = nonzeromsg(CMD_ISO15693_Read(reader_address, P_FLAGS, TOTAL_BLOCK_OFFSET, 1, NULL, tb.d.buf),"CMD_ISO15693_Read");
	if (rs == 0) {
		bf_decrypt(key, tb);
		dbprintf("Read value=%d, crc=%d, expr=%d\n", tb.d.v.val, tb.d.v.inv, (uint16_t)(tb.d.v.val + tb.d.v.inv));
		if(((uint16_t)(tb.d.v.val + tb.d.v.inv)) != 0) {
			rs = ERR_VAL_CRC;
		}
	}
	return rs;
}

int selected_write_total(const char key[], TotalBlock &tb)
{
	int rs;
	dbprintf("Writing value=%d\n", tb.d.v.val);
	tb.d.v.inv = -tb.d.v.val;
	bf_encrypt(key, tb);
	rs = nonzeromsg(CMD_ISO15693_Write(reader_address, P_FLAGS, TOTAL_BLOCK_OFFSET, 1, NULL, tb.d.buf), "CMD_ISO15693_Write");
	
/*	bf_decrypt(key, tb);
	printf("#######\n");
	struct {
		uint32_t val[1];
	} d;
	d.val[0] = 100;
	printf("Init value=0x%8.8X\n", d.val[0]);
	DESEncrypt((const uint8_t*)key, (uint8_t*)&d, sizeof(d));
//	process(true, (const uint8_t*)key, strlen(key), (uint8_t*)&d, sizeof(d));
	printf("Encoded values=0x%8.8X\n", d.val[0]);
	DESDecrypt((const uint8_t*)key, (uint8_t*)&d, sizeof(d));
//	process(false, (const uint8_t*)key, strlen(key), (uint8_t*)&d, sizeof(d));
	printf("Decrypted value=0x%8.8X\n", d.val[0]);
	printf("#######\n");
*/	return rs;
}

const char *time2str(uint32_t timestamp) {
	time_t tt = timestamp;
	char *ds = asctime(localtime(&tt));
	ds[strlen(ds) - 1] = 0;
	return ds;
}
