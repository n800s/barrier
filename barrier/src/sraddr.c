#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "CVAPIV01_DESFire.h"
#include "confreader.h"
#include "breader.h"
#include "debug.h"
#include "errcode.h"

int main(int argc, char * const *argv)
{
	init_configuration(argc, argv, "a:");
	int rs = ERR_USAGE;
	if(new_reader_address >= 0)
	{
		rs = openSerial(serial_type, device_index);
		if(rs == 0) {
			unsigned char addr = new_reader_address;
			rs = SetDeviceAddress(reader_address, addr, 0, NULL);
		} else {
			printf("Open reader is unsuccessfull\n");
		}
	} else {
		printf("Usage: sraddr -a <new reader address>\n");
	}
	do_exit(rs);
	dbprintf("exiting\n");
}
