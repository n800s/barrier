#ifndef __AESCRYPT_H

#define __AESCRYPT_H

int process(bool encrypt, const uint8_t *key, int keysize, uint8_t *databuf, int bufsize);

#endif //__AESCRYPT_H
