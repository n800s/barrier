#ifndef __BREADER_H
#define __BREADER_H

#include <stdint.h>
#include "carto.h"

int openUSB(); //returns 0 on success
int openSerial(int type=2, int index=0); //returns 0 on success
void stopReader();
int tr_init_v1(uint64_t &uid, const char key[], CARD_TYPE ctype=CT_NONE, CARD_SUBTYPE csubtype=CST_NONE, long value=0, time_t tbegin=0, time_t tend=0);

int tr_bundle_read(const uint64_t &uid, const char *passes[], int passcount, DataBundle &bundle);

int tr_select(uint64_t *puid=NULL, int retry = 1);
int tr_unselect();

#endif //__BREADER_H
