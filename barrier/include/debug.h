#ifndef __DEBUG_H
#define __DEBUG_H

#include <stdio.h>

extern bool debugmode;
extern bool quietmode;

void dbprintf(const char *format, ...);
void dumpbuf(const unsigned char *buf, int buflen);

//if rs is not zero it dbprintfs funcname with rs
//returns rs
int nonzeromsg(int rs, const char funcname[]);

FILE *log_open(const char *logfile);
void log_close();
void dblog_rollover(bool force=false);

#endif //__DEBUG_H
