#ifndef _DESCRYPT_H
#define _DESCRYPT_H

void DESEncrypt( const unsigned char *Key, unsigned char *Msg, int size);
 
void DESDecrypt( const unsigned char *Key, unsigned char *Msg, int size);

#endif //_DESCRYPT_H
