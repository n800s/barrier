#ifndef __CARTO_H
#define __CARTO_H

#include <stdint.h>
#include <memory.h>
#include <time.h>

//first block - location
//if it is the first
//write second and then set first that the second is writtent
//end of transaction

enum CARD_TYPE {CT_NONE=0, CT_CHILD, CT_ADULT};
enum CARD_SUBTYPE {CST_NONE=0, CST_PERIODONLY};

struct CARTO_V1{
	uint8_t pp; //incremented after each transaction
	uint64_t uid;
	//in rub
	int16_t maxval;
	//period begin
	uint32_t tbegin;
	//period end
	uint32_t tend;
	//CARD_TYPE
	uint8_t ctype;
	//PERIOD_ONLY
	uint8_t csubtype;
} __attribute__((packed));

struct CARTO_V2{
	uint8_t pp;
	uint64_t uid;
	//in kop
	int32_t value;
//	uint8_t userid[5];
} __attribute__((packed));

union CARTO_V {
	CARTO_V1 v1;
	CARTO_V2 v2;
} __attribute__((packed));

struct CARTO {
	uint8_t v; //version
	CARTO_V d;
} __attribute__((packed));

//first block is for a number and for crc
#define FIRST_BLOCK_OFFSET 0
//second block is for total
#define TOTAL_BLOCK_OFFSET 1
#define BLOCK_SIZE 4
#define MAX_BLOCK_COUNT 12
#define BUNDLE_OFFSET 2

union CARTO_MEM {
	CARTO carto;
	uint8_t blocks[MAX_BLOCK_COUNT][BLOCK_SIZE];
	uint8_t buf[MAX_BLOCK_COUNT*BLOCK_SIZE];
	struct {
		uint32_t lbuf[MAX_BLOCK_COUNT/2];
		uint32_t rbuf[MAX_BLOCK_COUNT/2];
	} d;
} __attribute__((packed));

struct DataBundle {
	CARTO_MEM mem;
	DataBundle() {
		memset((void*)&mem, 0, sizeof(mem));
		passindex = -1;
	}
	CARTO_MEM mem_bk;
	void backup() {
		memcpy(mem_bk.buf, mem.buf, sizeof(mem_bk.buf));
	}
	void restore() {
		memcpy(mem.buf, mem_bk.buf, sizeof(mem.buf));
	}
	int passindex;
	uint16_t calc_crc();
};

struct FirstBlock {
	//location (1 if odd, 0 if even)
	uint8_t loc;
	uint16_t crc;
} __attribute__((packed));

struct TotalBlock {
	TotalBlock() {
		memset((void*)&d, 0, sizeof(d));
	}
	union {
		struct {
			//in rub
			int16_t val;
			//negative val for checking purpose
			int16_t inv;
		} v;
		uint8_t buf[sizeof(uint32_t)];
	} d;
} __attribute__((packed));

//encryption
void bf_encrypt(const char *key, TotalBlock &m);
void bf_decrypt(const char *key, TotalBlock &m);
void bf_encrypt(const char *key, CARTO_MEM &m);
void bf_decrypt(const char *key, CARTO_MEM &m);

int selected_write_carto(const char key[], DataBundle &db);
int selected_read_carto(const char *passes[], int passcount, DataBundle &db);

int selected_write_total(const char key[], TotalBlock &tb);
int selected_read_total(const char key[], TotalBlock &tb);

const char *time2str(uint32_t timestamp);

#endif //__CARTO_H
