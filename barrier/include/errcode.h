#ifndef _ERRCODE_H_
#define _ERRCODE_H_

#define ERR_USAGE -1
#define ERR_FAKE_CARD -20
#define ERR_VAL_LIMIT -21
#define ERR_CRC -22
#define ERR_CTYPE -23
#define ERR_CSUBTYPE -24
#define ERR_VAL_CRC -25
#define ERR_AES -26

#endif //_ERRCODE_H_
