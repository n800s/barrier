#ifndef __CONFREADER_H

#define __CONFREADER_H

#include <time.h>
#include <stdio.h>
#include <stdint.h>

extern char configfile[256];

extern int adult_price;
extern int child_price;
extern int bond_value;
extern int d_timeout;

//maximum size of log file after which a rollover occurs
extern long max_log_size;
extern int max_log_count;

extern long max_transact_size;
extern long max_error_size;

extern bool everyhour_rollover_mode;

extern char errfile[256];
extern char dbfile[256];
extern char logfile[256];

enum EVENT_TYPE {CHILD_PAYMENT=0, ADULT_PAYMENT, CHILD_TIME_CHECK, ADULT_TIME_CHECK, ERROR_EVENT};
extern char events[][100];

extern bool usb_mode;
//interface device for interface Serial 0 - ttyS, 1 - ttyUSB, 2 - ttyM
extern int serial_type;
//interface device index - ttyM[0], ttyM[1] ...
extern int device_index;

//card uid
//if 0 then any card
extern uint64_t card_uid;

//global reader address = 0
extern int reader_address;

//new reader address to set = -1 (none)
extern int new_reader_address;

//card type (adult, child)
extern char ctype_str[20];

//card subtype (periodonly)
extern char csubtype_str[20];

//value to add
extern long value2add;
extern bool has_value2add;

//value to set
extern long value2set;
extern bool has_value2set;

//time begin format YYYYMMDDHHMI
extern time_t tbegin;
extern time_t tend;

int update_config(const char *configfile);
int process_common_options(int argc, char *const *argv, const char *opts=NULL);
int do_exit(int exitcode);

void init_configuration(int argc, char *const *argv, const char *opts=NULL);

FILE *fopen_split(const char *filename, const char *mode, long max_size, const char *titles=NULL, bool force=false);

#endif //__CONFREADER_H
