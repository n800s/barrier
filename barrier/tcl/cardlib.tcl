#!/usr/bin/env tclsh

namespace eval ::cardlib {

	proc init binpath {
		variable bpath
		set bpath $binpath
		set ::env(LD_LIBRARY_PATH) $binpath
	}

	proc run_cmd cmd {
		array set rs {-error {}}
		puts "\"$cmd\""
		set output [eval exec -ignorestderr -keepnewline -- $cmd]
		foreach line [split $output "\n"] {
			puts Line:$line
			if [string match "IFACE:*" $line] {
				set linelist [split [string range $line [string length "IFACE:"] end] =]
				set rs([lindex $linelist 0]) [lindex $linelist 1]
			}	elseif [string match "IERROR:*" $line] {
				lappend rs(-error) [string range $line [string length "IERROR:"] end]
			} else {
			  puts stderr $line
			}
		}
		mputs Returned:[array get rs]
		return [array get rs]
	}

	proc cardinfo {} {
		variable bpath
		return [run_cmd "$bpath/cardinfo -i -s \"$bpath/barrier.conf\""]
	}

	#date format YYYYMMDDhhmm
	proc cardreset {{uid {}} {type {}} {subtype {}} {val {}} {bdate {}} {edate {}}} {
		variable bpath
		set params [list]
		if { $uid != {} } { lappend params "-n \"$uid\"" }
		if { $type != {} } { lappend params "-t \"$type\"" }
		if { $subtype != {} } { lappend params "-p \"$subtype\"" }
		if { $val != {} } { lappend params "-V \"$val\"" }
		if { $bdate != {} } {
			set bdate [clock format $bdate -format %Y%m%d%H%M]
			lappend params "-b \"$bdate\"" 
		}
		if { $edate != {} } { 
			set edate [clock format $edate -format %Y%m%d%H%M]
			lappend params "-e \"$edate\"" 
		}
		return [run_cmd "$bpath/cardreset -i -s \"$bpath/barrier.conf\" [join $params]"]
	}

	proc addvalue val {
		variable bpath
		if { $val >= 0 } {
		  set val "+$val"
		}
		return [run_cmd "$bpath/addvalue -i -s \"$bpath/barrier.conf\" -v \"$val\""]
	}
}

#::cardlib::init ../build/x86

#puts [::cardlib::addvalue 5]
#puts [::cardlib::cardreset 5 201211110101]
#puts [::cardlib::addvalue +5]
#puts [::cardlib::addvalue -5]
#puts [::cardlib::cardinfo]

