#!/usr/bin/env wish

# Начало настроек

# каталог, где находится cardinfo
set ::toolpath ../build/fake

# каталог с видео файлами
set ::videopath ../../../video

# размер шрифта
set ::fontsize 40

# время между тем когда карточку считывают и когда видео продолжится (в секундах)
set ::timeout2continue 10

# Конец настроек

# fullscreen mode - set :;fullscreen to 1
# windowed mode - set :;fullscreen to 0
set ::fullscreen 1

#uid of last card
set ::last_uid {}

set ::start_geometry 800x600+100+100

set ::bigfont [font create -size $::fontsize -family Courier]

proc bgerror message {
	puts stderr "An event-based script faulted with '$message'."
	puts $::errorInfo
}

proc wait_for {ch param} {
	for {
		set line {}
	} {
		[lindex $line 0] != $param
	} {
		set line [gets $ch]
		set line [split $line =]
	} { puts LINE=$line }
	return [lindex $line 1]
}

proc check_mp_running {} {
	set rs [is_mp_open]
	if { !$rs } {
		run_mp
	}
	return $rs
}

proc is_mp_open {} {
	set opened [file channels $::mp]
	puts "Opened $opened"
	return [expr {$opened != {}}]
}

proc hide_mp {} {
	wm attributes .mp -fullscreen 0 -topmost 0
	wm iconify .mp
	wm deiconify .
	wm attributes . -fullscreen $::fullscreen -topmost 1
}

proc show_mp {} {
	wm attributes . -fullscreen 0 -topmost 0
	wm iconify .
	wm deiconify .mp
	puts deiconify
	wm attributes .mp -fullscreen $::fullscreen -topmost 1
}

proc run_mp {} {
	puts run
	set wid [winfo id .mp]
	show_mp
	set flist [glob "$::videopath/*"]
#	set mpcmd "|mplayer -loop 0 -slave [join $flist { }]"
	set mpcmd "|mplayer -loop 0 -slave -quiet -wid $wid [join $flist { }]"
#	set mpcmd "find $::videopath|xargs mplayer -vo x11 -loop 0 -slave -wid $wid"
#	set mpcmd "|ls [join $flist { }]"
	puts $mpcmd
	set ::mp [open $mpcmd w]
	set ::is_paused 0
}

proc pause_mp {} {
	puts pause
	if { ! $::is_paused } {
		check_mp_running
		hide_mp
		puts {send pause}
		puts $::mp pause
		flush $::mp
		set ::is_paused 1
	}
}

proc continue_mp {} {
	puts continue
	for { set i 1 } { $i <= 5 } { incr i } {
		.m$i configure -text {}
	}
	if { $::is_paused } {
		puts cont1
		if [check_mp_running] {
			puts cont2
			puts {send pause to continue}
			puts $::mp pause
			flush $::mp
			set ::is_paused 0
		}
		show_mp
	}
}

proc add_file fname {
	puts "add file $fname"
	check_mp_running
	puts $::mp loadfile $fname
	flush $::mp
}

proc check_card {} {
	set rs 0
	set card {}
	set cmd "/usr/bin/env LD_LIBRARY_PATH=${::toolpath}:\${LD_LIBRARY_PATH} ${::toolpath}/cardinfo -i"
#	puts $cmd
	set f [open "|$cmd" r]
	while { ![eof $f] } {
		set l [gets $f]
		if [string match {IFACE:*} $l] {
			set l [string range $l 6 end]
			set card [concat $card [split $l =]]
		}
		if [string match {IERROR:*} $l] {
			set l [string range $l 7 end]
			puts stderr "Error:$l"
		}
	}
	if [catch {close $f} err] {
		puts stderr "cardinfo error: $err, code: $::errorCode"
	}
#puts >>>$card
	array set acard $card
#TYPE periodonly VALUE 10000 STARTTIME 1381582800 TYPE adult ENDTIME 1382187600 UID E00401007C862A6F MAXVALUE 10000
	if [info exists acard(UID)] {
		if { $acard(TYPE) == {child} } {
			.m1 configure -text Детский
		} else {
			.m1 configure -text {}
		}
		set dateformat {%d.%m.%Y %H:%M}
		if { $acard(SUBTYPE) == {periodonly} } {
			.m2 configure -text {Период действия}
			.m3 configure -text "[clock format $acard(STARTTIME) -format $dateformat] .. [clock format $acard(ENDTIME) -format $dateformat]"
			set rsec [expr $acard(ENDTIME) - [clock scan now]]
			set rdays [expr $rsec / 3600 / 24] 
			set rh [expr ( $rsec - $rdays * 3600 * 24 ) / 3600]
			.m4 configure -text "Осталось $rdays дней $rh часов"
			.m5 configure -text {}
		} else {
			.m2 configure -text "Остаток на карточке $acard(VALUE)"
			.m3 configure -text {Срок действия карточки закончится}
			.m4 configure -text "[clock format $acard(ENDTIME) -format $dateformat]"
			.m5 configure -text {}
		}
		if { $acard(UID) != $::last_uid } {
			set rs 1
			# new card
			set ::last_uid $acard(UID)
		}
	} else {
		set ::last_uid {}
	}
#	set rs [expr { rand() >= 0.9 }]
#	puts "cardinfo status $rs"
	return $rs
}

proc start {} {
	run_mp
	set ::last_err {}
	set ::timerh {}
	single_act
}

proc single_act {} {
	if [catch {
		if { [check_card] } {
				after cancel $::timerh
				pause_mp
				set ::timerh [after [expr $::timeout2continue * 1000] continue_mp]
		}
		if { $::last_err != {} } {
			puts stderr $::last_err
			set ::last_err {}
		}
	} err] {
		if { $::last_err != $err } {
			puts stderr $err
			puts stderr $::errorInfo
		}
		set ::last_err $err
	}
	after 500 single_act
}

proc make_controls {} {

	wm geometry . $::start_geometry
	wm title . Informer
	. configure -bg yellow

	label .m1 -text {} -background white -font $::bigfont
	label .m2 -text {} -background white -font $::bigfont
	label .m3 -text {} -background white -font $::bigfont
	label .m4 -text {} -background white -font $::bigfont
	label .m5 -text {} -background white -font $::bigfont
	pack .m1 -expand true -fill both -ipadx 10 -ipady 10
	pack .m2 -expand true -fill both -ipadx 10 -ipady 10
	pack .m3 -expand true -fill both -ipadx 10 -ipady 10
	pack .m4 -expand true -fill both -ipadx 10 -ipady 10
	pack .m5 -expand true -fill both -ipadx 10 -ipady 10

	toplevel .mp
	wm geometry .mp $::start_geometry
	wm title .mp Player
	.mp configure -bg blue
#	wm attributes .mp -fullscreen 1

}

make_controls
after idle start
