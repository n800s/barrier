/**
  AES encryption/decryption demo program using OpenSSL EVP apis
  gcc -Wall openssl_aes.c -lcrypto

  this is public domain code. 

  Saju Pillai (saju.pillai@gmail.com)
**/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <openssl/evp.h>
#include <openssl/aes.h>

/**
 * Create an 256 bit key and IV using the supplied key_data. salt can be added for taste.
 * Fills in the encryption and decryption ctx objects and returns 0 on success
 **/
int aes_init(unsigned char *key_data, int key_data_len, unsigned char *salt, EVP_CIPHER_CTX *e_ctx, 
             EVP_CIPHER_CTX *d_ctx)
{
  int i, nrounds = 5;
  unsigned char key[32], iv[32];
  
  /*
   * Gen key & IV for AES 256 CBC mode. A SHA1 digest is used to hash the supplied key material.
   * nrounds is the number of times the we hash the material. More rounds are more secure but
   * slower.
   */
  i = EVP_BytesToKey(EVP_aes_256_cbc(), EVP_sha1(), salt, key_data, key_data_len, nrounds, key, iv);
  if (i != 32) {
    printf("Key size is %d bits - should be 256 bits\n", i);
    return -1;
  }

  EVP_CIPHER_CTX_init(e_ctx);
  EVP_EncryptInit_ex(e_ctx, EVP_aes_256_cbc(), NULL, key, iv);
  EVP_CIPHER_CTX_init(d_ctx);
  EVP_DecryptInit_ex(d_ctx, EVP_aes_256_cbc(), NULL, key, iv);

  return 0;
}

/*
 * Encrypt *len bytes of data
 * All data going in & out is considered binary (unsigned char[])
 */
uint8_t *aes_encrypt(EVP_CIPHER_CTX *e, uint8_t *textbuf, int buflen)
{
  /* max ciphertext len for a n bytes of plaintext is n + AES_BLOCK_SIZE -1 bytes */
  int c_len = buflen + AES_BLOCK_SIZE, f_len = 0;
  uint8_t *ciphertext = malloc(c_len);

  /* allows reusing of 'e' for multiple encryption cycles */
  EVP_EncryptInit_ex(e, NULL, NULL, NULL, NULL);

  /* update ciphertext, c_len is filled with the length of ciphertext generated,
    *len is the size of plaintext in bytes */
  EVP_EncryptUpdate(e, ciphertext, &c_len, textbuf, buflen);

  /* update ciphertext with the final remaining bytes */
  EVP_EncryptFinal_ex(e, ciphertext+c_len, &f_len);

  memcpy(textbuf, ciphertext, buflen);
	free(ciphertext);
  return textbuf;
}

/*
 * Decrypt *len bytes of ciphertext
 */
unsigned char *aes_decrypt(EVP_CIPHER_CTX *e, unsigned char *ciphertext, int buflen)
{
  /* because we have padding ON, we must allocate an extra cipher block size of memory */
  int p_len = buflen, f_len = 0;
  uint8_t *plaintext = malloc(p_len + AES_BLOCK_SIZE);
  
  EVP_DecryptInit_ex(e, NULL, NULL, NULL, NULL);
  EVP_DecryptUpdate(e, plaintext, &p_len, ciphertext, buflen);
  EVP_DecryptFinal_ex(e, plaintext+p_len, &f_len);

  memcpy(ciphertext, plaintext, buflen);
	free(plaintext);
  return ciphertext;
}

int main(int argc, char **argv)
{
  /* "opaque" encryption, decryption ctx structures that libcrypto uses to record
     status of enc/dec operations */
  EVP_CIPHER_CTX en, de;

  /* 8 bytes to salt the key_data during key generation. This is an example of
     compiled in salt. We just read the bit pattern created by these two 4 byte 
     integers on the stack as 64 bits of contigous salt material - 
     ofcourse this only works if sizeof(int) >= 4 */
  uint8_t salt[8] = {0xD1, 0x31, 0x0B, 0xA6, 0x98, 0xDF, 0xB5, 0xAC};
  uint8_t *key_data = (uint8_t*)"hello";
  int key_data_len = strlen(argv[1]);
  int i;
  uint8_t input[] = "123456789012345678901234567890123456789012345678";
  
  /* gen key and iv. init the cipher ctx object */
  if (aes_init(key_data, key_data_len, salt, &en, &de)) {
    printf("Couldn't initialize AES cipher\n");
    return -1;
  }

	printf("Input: \"%s\" len:%ld\n", input, sizeof(input)/4);
  /* encrypt and decrypt each input string and compare with the original */
   aes_encrypt(&en, input, sizeof(input)-1);
		for(i=0; i<sizeof(input)-1; i++) {
			printf("%.2X ", input[i]);
		}
		printf("\n\n");
    
    /* The enc/dec functions deal with binary data and not C strings. strlen() will 
       return length of the string without counting the '\0' string marker. We always
       pass in the marker byte to the encrypt/decrypt functions so that after decryption 
       we end up with a legal C string */
    
    aes_decrypt(&de, input, sizeof(input)-1);

	printf("Output: \"%s\"\n", input);

  EVP_CIPHER_CTX_cleanup(&en);
  EVP_CIPHER_CTX_cleanup(&de);

  return 0;
}
  

